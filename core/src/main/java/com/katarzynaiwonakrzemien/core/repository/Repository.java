package com.katarzynaiwonakrzemien.core.repository;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;

import java.util.List;

import io.reactivex.Observable;

public interface Repository {

    Observable<List<Budget>> budgets();

    Observable<List<User>> users();

    void addUserIfNotExist(User user);

    void addBudget(Budget budget);

    void addSpending(Spending spending);

    void addDebt(Debt debt);

    void deleteBudget(Budget budget);

    void deleteSpending(String id);

    void deleteDebt(String id);

    void updateDebt(Debt debt);

    void updateBudget(Budget budget);

    void updateSpending(Spending spending);

    void refreshBudgets();

    void refreshUsers();
}
