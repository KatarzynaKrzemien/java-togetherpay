package com.katarzynaiwonakrzemien.core.repository;

public interface RepositoryManager extends Repository {
    void cleanData();
}
