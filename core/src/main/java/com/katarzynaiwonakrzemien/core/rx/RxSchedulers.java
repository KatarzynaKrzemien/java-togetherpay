package com.katarzynaiwonakrzemien.core.rx;

import io.reactivex.Scheduler;

public interface RxSchedulers {

    Scheduler getBackground();

    Scheduler getUi();
}
