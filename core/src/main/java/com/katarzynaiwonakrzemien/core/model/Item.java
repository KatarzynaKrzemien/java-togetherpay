package com.katarzynaiwonakrzemien.core.model;

import java.util.HashMap;
import java.util.UUID;

public abstract class Item {

    protected final String id;

    static String defaultId() {
        return String.valueOf(UUID.randomUUID().hashCode());
    }

    Item(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public abstract HashMap<String, Object> toHashMap();

}
