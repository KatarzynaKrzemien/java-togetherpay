package com.katarzynaiwonakrzemien.core.model;

import java.util.HashMap;

public class Debt extends Item {

    public static final int NOREPAY = 0;
    public static final int CONFIRMED = 1;
    public static final int WAITING = 2;
    public static final int REJECTED = 3;

    private final String idBudget;
    private final String idUser;
    private final String nameUser;
    private final int status;
    private final String updateDate;
    private final double amount;
    private final String date;

    public Debt(String id, String idBudget, String idUser, String nameUser, int status, String updateDate, double amount, String date) {
        super(id);
        this.idBudget = idBudget;
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.status = status;
        this.updateDate = updateDate;
        this.amount = amount;
        this.date = date;
    }

    public Debt(String idBudget, String idUser, String nameUser, String updateDate, double amount, String date) {
        super(Item.defaultId());
        this.idBudget = idBudget;
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.status = WAITING;
        this.updateDate = updateDate;
        this.amount = amount;
        this.date = date;
    }

    public Debt(String idBudget, String idUser, String nameUser, int status, double amount) {
        super(Item.defaultId());
        this.idBudget = idBudget;
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.status = status;
        this.updateDate = "";
        this.amount = amount;
        this.date = "";
    }

    public Debt() {
        super(Item.defaultId());
        this.idBudget = "";
        this.idUser = Item.defaultId();
        this.nameUser = "";
        this.status = WAITING;
        this.updateDate = "";
        this.amount = 0.0;
        this.date = "";
    }

    public String getIdBudget() {
        return idBudget;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public int getStatus() {
        return status;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public double getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }

    @Override
    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", id);
        hashMap.put("idBudget", idBudget);
        hashMap.put("idUser", idUser);
        hashMap.put("nameUser", nameUser);
        hashMap.put("status", status);
        hashMap.put("updateDate", updateDate);
        hashMap.put("amount", amount);
        hashMap.put("date", date);
        return hashMap;
    }
}
