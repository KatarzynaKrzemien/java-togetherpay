package com.katarzynaiwonakrzemien.core.model;

import java.util.HashMap;

public class User extends Item {

    private final String name;

    public User(String id, String name) {
        super(id);
        this.name = name;
    }

    public User() {
        super(defaultId());
        this.name = "";
    }

    public String getName() {
        return name;
    }

    @Override
    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", id);
        hashMap.put("name", name);
        return hashMap;
    }
}
