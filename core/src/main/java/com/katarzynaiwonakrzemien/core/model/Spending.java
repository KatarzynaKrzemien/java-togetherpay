package com.katarzynaiwonakrzemien.core.model;

import java.util.HashMap;

public class Spending extends Item {

    private final String idBudget;
    private final String idUser;
    private final String nameUser;
    private final String product;
    private final double amount;
    private final String date;

    public Spending(String id, String idBudget, String idUser, String nameUser, String product, double amount, String date) {
        super(id);
        this.idBudget = idBudget;
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.product = product;
        this.amount = amount;
        this.date = date;
    }

    public Spending(String idBudget, String idUser, String nameUser, String product, double amount, String date) {
        super(Item.defaultId());
        this.idBudget = idBudget;
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.product = product;
        this.amount = amount;
        this.date = date;
    }

    public Spending() {
        super(Item.defaultId());
        this.idBudget = "";
        this.idUser = "";
        this.nameUser = "";
        this.product = "";
        this.amount = 0.0;
        this.date = "";
    }

    public String getIdBudget() {
        return idBudget;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public String getProduct() {
        return product;
    }

    public double getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }

    @Override
    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", id);
        hashMap.put("idBudget", idBudget);
        hashMap.put("idUser", idUser);
        hashMap.put("nameUser", nameUser);
        hashMap.put("product", product);
        hashMap.put("amount", amount);
        hashMap.put("date", date);
        return hashMap;
    }
}
