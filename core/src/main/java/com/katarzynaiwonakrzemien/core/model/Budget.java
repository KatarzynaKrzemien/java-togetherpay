package com.katarzynaiwonakrzemien.core.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Budget extends Item {

    private final String name;
    private final String firstUserId;
    private final String firstUserName;
    private final String secondUserId;
    private final String secondUserName;
    private final List<Spending> spending;
    private final List<Debt> repaidDebts;

    public Budget(String id, String name, String firstUserId, String firstUserName, String secondUserId, String secondUserName, List<Spending> spending, List<Debt> repaidDebts) {
        super(id);
        this.name = name;
        this.firstUserId = firstUserId;
        this.firstUserName = firstUserName;
        this.secondUserId = secondUserId;
        this.secondUserName = secondUserName;
        this.spending = spending;
        this.repaidDebts = repaidDebts;
    }

    public Budget(String name, String firstUserId, String firstUserName, String secondUserId, String secondUserName) {
        super(Item.defaultId());
        this.name = name;
        this.firstUserId = firstUserId;
        this.firstUserName = firstUserName;
        this.secondUserId = secondUserId;
        this.secondUserName = secondUserName;
        this.spending = new ArrayList<>();
        this.repaidDebts = new ArrayList<>();
    }

    public Budget() {
        super(Item.defaultId());
        this.name = "";
        this.firstUserId = "";
        this.firstUserName = "";
        this.secondUserId = "";
        this.secondUserName = "";
        this.spending = new ArrayList<>();
        this.repaidDebts = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getFirstUserId() {
        return firstUserId;
    }

    public String getFirstUserName() {
        return firstUserName;
    }

    public String getSecondUserId() {
        return secondUserId;
    }

    public String getSecondUserName() {
        return secondUserName;
    }

    public List<Spending> getSpending() {
        return spending;
    }

    public List<Debt> getRepaidDebts() {
        return repaidDebts;
    }

    @Override
    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", id);
        hashMap.put("name", name);
        hashMap.put("firstUserId", firstUserId);
        hashMap.put("firstUserName", firstUserName);
        hashMap.put("secondUserId", secondUserId);
        hashMap.put("secondUserName", secondUserName);
        return hashMap;
    }
}
