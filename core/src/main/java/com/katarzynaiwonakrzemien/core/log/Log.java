package com.katarzynaiwonakrzemien.core.log;

public interface Log {

    // static String getTAG(Object object) {
    //     return object.getClass().getSimpleName();
    // }

    void d(String tag, String message);

    void d(String message);

    void w(String tag, String message, Throwable throwable);

    void w(String tag, String message);

    void w(String message, Throwable throwable);

    void w(String message);

    void e(String tag, String message, Throwable throwable);

    void e(String tag, String message);

    void e(String message, Throwable throwable);

    void e(String message);

    void i(String tag, String message);

    void i(String message);

    void wtf(String tag, String message);

    void wtf(String message);

}
