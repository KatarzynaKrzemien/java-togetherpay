package com.katarzynaiwonakrzemien.domain;

import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public abstract class BasePresenter<E extends BaseContract.Event, A extends BaseContract.Action> implements BaseContract.Presenter<E, A> {

    protected final RxSchedulers schedulers;
    private final CompositeDisposable disposables = new CompositeDisposable();
    protected final Subject<E> eventSubject = PublishSubject.create();

    public BasePresenter(RxSchedulers schedulers) {
        this.schedulers = schedulers;
    }

    protected Boolean addDisposable(Disposable disposable) {
        return disposables.add(disposable);
    }

    public abstract void consume(A action);

    public void send(E event) {
        eventSubject.onNext(event);
    }

    @Override
    public void attach(Observable<A> view) {
        addDisposable(view.subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(a -> consume(a)));
    }

    @Override
    public Observable<E> subscribe() {
        return eventSubject;
    }

    @Override
    public void dispose() {
        disposables.clear();
    }
}
