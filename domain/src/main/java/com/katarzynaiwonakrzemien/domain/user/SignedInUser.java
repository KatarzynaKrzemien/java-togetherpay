package com.katarzynaiwonakrzemien.domain.user;

import com.katarzynaiwonakrzemien.core.model.User;

public interface SignedInUser {

    void signOut();

    void signIn(User user);

    User getUser();
}
