package com.katarzynaiwonakrzemien.domain.transformer.debt;

import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi.Status;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;

public class DebtUiToItemTransformer implements UiToItemTransformer<DebtUi, Debt> {

    @Override
    public Debt transform(DebtUi uiItem) {
        return new Debt(
                uiItem.getId(),
                uiItem.getIdBudget(),
                uiItem.getUser().getId(),
                uiItem.getUser().getName(),
                transformDebtUiStatusToItem(uiItem.getStatus()),
                uiItem.getUpdateDate().getTime(),
                uiItem.getAmount().getAmount(),
                uiItem.getDate().getTime()
        );
    }

    private int transformDebtUiStatusToItem(Status status) {
        switch (status) {
            case CONFIRMED:
                return Debt.CONFIRMED;
            case NOREPAY:
                return Debt.NOREPAY;
            case REJECTED:
                return Debt.REJECTED;
            case WAITING:
                return Debt.WAITING;
            default:
                throw new IllegalArgumentException();
        }
    }
}
