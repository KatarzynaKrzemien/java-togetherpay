package com.katarzynaiwonakrzemien.domain.transformer.debt;

import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi;
import com.katarzynaiwonakrzemien.domain.model.DateUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;

public class DebtItemToUiTransformer implements ItemToUiTransformer<Debt, DebtUi> {

    private final User signedInUser;
    private final ItemToUiTransformer<User, UserUi> userItemToUiTransformer;

    public DebtItemToUiTransformer(User signedInUser, ItemToUiTransformer<User, UserUi> userItemToUiTransformer) {
        this.signedInUser = signedInUser;
        this.userItemToUiTransformer = userItemToUiTransformer;
    }

    @Override
    public DebtUi transform(Debt item) {
        return new DebtUi(
                item.getId(),
                userItemToUiTransformer.transform(new User(item.getIdUser(), item.getNameUser())),
                item.getIdBudget(),
                transformDebtStatusToUi(item.getStatus()),
                new DateUi(item.getUpdateDate()),
                new AmountInfoUi(item.getAmount(), item.getIdUser().equals(signedInUser.getId())),
                new DateUi(item.getDate())
        );
    }

    private DebtUi.Status transformDebtStatusToUi(int status) {
        switch (status) {
            case Debt.CONFIRMED:
                return DebtUi.Status.CONFIRMED;
            case Debt.NOREPAY:
                return DebtUi.Status.NOREPAY;
            case Debt.REJECTED:
                return DebtUi.Status.REJECTED;
            case Debt.WAITING:
                return DebtUi.Status.WAITING;
            default:
                throw new IllegalArgumentException();
        }
    }
}
