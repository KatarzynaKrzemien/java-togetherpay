package com.katarzynaiwonakrzemien.domain.transformer.spending;

import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;

public class SpendingUiToItemTransformer implements UiToItemTransformer<SpendingUi, Spending> {

    @Override
    public Spending transform(SpendingUi uiItem) {
        return new Spending(
                uiItem.getId(),
                uiItem.getIdBudget(),
                uiItem.getUser().getId(),
                uiItem.getUser().getName(),
                uiItem.getProduct(),
                uiItem.getAmount().getAmount(),
                uiItem.getDate().getTime()
        );
    }
}
