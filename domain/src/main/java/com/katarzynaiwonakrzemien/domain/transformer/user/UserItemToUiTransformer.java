package com.katarzynaiwonakrzemien.domain.transformer.user;

import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;

public class UserItemToUiTransformer implements ItemToUiTransformer<User, UserUi> {

    @Override
    public UserUi transform(User item) {
        return new UserUi(item.getId(), item.getName());
    }
}