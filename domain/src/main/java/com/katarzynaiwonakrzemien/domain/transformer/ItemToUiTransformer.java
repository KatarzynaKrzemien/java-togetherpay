package com.katarzynaiwonakrzemien.domain.transformer;

import com.katarzynaiwonakrzemien.core.model.Item;
import com.katarzynaiwonakrzemien.domain.model.ItemUi;

public interface ItemToUiTransformer<T1 extends Item, T2 extends ItemUi> {
    T2 transform(T1 item);
}
