package com.katarzynaiwonakrzemien.domain.transformer.budget;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;

import java.util.ArrayList;
import java.util.List;

public class BudgetItemToUiTransformer implements ItemToUiTransformer<Budget, BudgetUi> {

    private final User signedInUser;
    private final ItemToUiTransformer<User, UserUi> userItemToUiTransformer;
    private final ItemToUiTransformer<Spending, SpendingUi> spendingItemToUiTransformer;
    private final ItemToUiTransformer<Debt, DebtUi> debtItemToUiTransformer;

    public BudgetItemToUiTransformer(User signedInUser, ItemToUiTransformer<User, UserUi> userItemToUiTransformer, ItemToUiTransformer<Spending, SpendingUi> spendingItemToUiTransformer, ItemToUiTransformer<Debt, DebtUi> debtItemToUiTransformer) {
        this.signedInUser = signedInUser;
        this.userItemToUiTransformer = userItemToUiTransformer;
        this.spendingItemToUiTransformer = spendingItemToUiTransformer;
        this.debtItemToUiTransformer = debtItemToUiTransformer;
    }

    @Override
    public BudgetUi transform(Budget item) {
        return new BudgetUi(
                item.getId(),
                item.getName(),
                userItemToUiTransformer.transform(getOtherUser(item)),
                sumSpending(item.getSpending()),
                calculateDebt(
                        calculateUserSpending(signedInUser.getId(), item.getSpending(), item.getRepaidDebts()),
                        calculateUserSpending(getOtherUser(item).getId(), item.getSpending(), item.getRepaidDebts())
                ),
                transformSpendingToSpendingUi(item.getSpending()),
                transformDebToDebtUi(item.getRepaidDebts())
        );
    }

    private User getOtherUser(Budget item) {
        if (item.getFirstUserId().equals(signedInUser.getId())) {
            return new User(item.getSecondUserId(), item.getSecondUserName());
        } else {
            return new User(item.getFirstUserId(), item.getFirstUserName());
        }
    }

    private double sumSpending(List<Spending> spendingList) {
        double sum = 0.0;
        for (Spending spending : spendingList) {
            sum += spending.getAmount();
        }
        return sum;
    }

    private AmountInfoUi calculateDebt(double signedInUserSpending, double otherUserSpending) {
        return new AmountInfoUi(Math.abs(signedInUserSpending - otherUserSpending), signedInUserSpending < otherUserSpending);
    }

    private double calculateUserSpending(String idUser, List<Spending> spendingList, List<Debt> repaidDebts) {
        double userSpending = 0.0;
        for (Spending spending : spendingList) {
            if (spending.getIdUser().equals(idUser)) {
                userSpending += spending.getAmount();
            }
        }
        userSpending /= 2;
        for (Debt debt : repaidDebts) {
            if (debt.getIdUser().equals(idUser)) {
                if (debt.getStatus() == Debt.CONFIRMED || debt.getStatus() == Debt.WAITING) {
                    userSpending += debt.getAmount();
                }
            }
        }
        return userSpending;

    }

    private List<SpendingUi> transformSpendingToSpendingUi(List<Spending> spendingList) {
        List<SpendingUi> spendingUiList = new ArrayList<>();
        for (Spending spending : spendingList) {
            spendingUiList.add(spendingItemToUiTransformer.transform(spending));
        }
        return spendingUiList;
    }

    private List<DebtUi> transformDebToDebtUi(List<Debt> debtList) {
        List<DebtUi> debtUiList = new ArrayList<>();
        for (Debt debt : debtList) {
            debtUiList.add(debtItemToUiTransformer.transform(debt));
        }
        return debtUiList;
    }
}
