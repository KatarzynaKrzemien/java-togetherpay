package com.katarzynaiwonakrzemien.domain.transformer.budget;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;

import java.util.ArrayList;
import java.util.List;

public class BudgetUiToItemTransformer implements UiToItemTransformer<BudgetUi, Budget> {

    private final User signedInUser;
    private final UiToItemTransformer<SpendingUi, Spending> spendingUiToItemTransformer;
    private final UiToItemTransformer<DebtUi, Debt> debtUiToItemTransformer;

    public BudgetUiToItemTransformer(User user, UiToItemTransformer<SpendingUi, Spending> spendingUiToItemTransformer, UiToItemTransformer<DebtUi, Debt> debtUiToItemTransformer) {
        this.signedInUser = user;
        this.spendingUiToItemTransformer = spendingUiToItemTransformer;
        this.debtUiToItemTransformer = debtUiToItemTransformer;
    }

    @Override
    public Budget transform(BudgetUi uiItem) {
        return new Budget(
                uiItem.getId(),
                uiItem.getName(),
                signedInUser.getId(),
                signedInUser.getName(),
                uiItem.getOtherUser().getId(),
                uiItem.getOtherUser().getName(),
                transformSpendingUiToSpending(uiItem.getSpendingList()),
                transformDebUiToDebt(uiItem.getRepaidDebts())
        );
    }

    private List<Spending> transformSpendingUiToSpending(List<SpendingUi> spendingUiList) {
        List<Spending> spendingList = new ArrayList<>();
        for (SpendingUi spending : spendingUiList) {
            spendingList.add(spendingUiToItemTransformer.transform(spending));
        }
        return spendingList;
    }

    private List<Debt> transformDebUiToDebt(List<DebtUi> debtUiList) {
        List<Debt> debtList = new ArrayList<>();
        for (DebtUi debt : debtUiList) {
            debtList.add(debtUiToItemTransformer.transform(debt));
        }
        return debtList;
    }
}
