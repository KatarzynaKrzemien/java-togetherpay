package com.katarzynaiwonakrzemien.domain.transformer;

import com.katarzynaiwonakrzemien.core.model.Item;
import com.katarzynaiwonakrzemien.domain.model.ItemUi;

public interface UiToItemTransformer<T1 extends ItemUi, T2 extends Item> {
    T2 transform(T1 uiItem);
}
