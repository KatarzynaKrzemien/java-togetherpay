package com.katarzynaiwonakrzemien.domain.transformer.spending;

import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi;
import com.katarzynaiwonakrzemien.domain.model.DateUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;

public class SpendingItemToUiTransformer implements ItemToUiTransformer<Spending, SpendingUi> {

    private final User signedInUser;
    private final ItemToUiTransformer<User, UserUi> userItemToUiTransformer;

    public SpendingItemToUiTransformer(User signedInUser, ItemToUiTransformer<User, UserUi> userItemToUiTransformer) {
        this.signedInUser = signedInUser;
        this.userItemToUiTransformer = userItemToUiTransformer;
    }

    @Override
    public SpendingUi transform(Spending item) {
        return new SpendingUi(
                item.getId(),
                item.getIdBudget(),
                userItemToUiTransformer.transform(new User(item.getIdUser(), item.getNameUser())),
                item.getProduct(),
                new AmountInfoUi(item.getAmount(), item.getIdUser().equals(signedInUser.getId())),
                new DateUi(item.getDate())
        );
    }
}
