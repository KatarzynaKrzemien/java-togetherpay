package com.katarzynaiwonakrzemien.domain.transformer.user;

import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;

public class UserUiToItemTransformer implements UiToItemTransformer<UserUi, User> {

    @Override
    public User transform(UserUi uiItem) {
        return new User(uiItem.getId(), uiItem.getName());
    }
}
