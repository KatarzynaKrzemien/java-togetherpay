package com.katarzynaiwonakrzemien.domain.contract.budget;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BasePresenter;

public class SelectedBudgetPresenter extends BasePresenter<SelectedBudgetContract.Event, SelectedBudgetContract.Action> implements SelectedBudgetContract.Presenter {

    private final RepositoryManager repository;
    private final User signedUser;

    public SelectedBudgetPresenter(RxSchedulers schedulers, RepositoryManager repository, User signedUser) {
        super(schedulers);
        this.repository = repository;
        this.signedUser = signedUser;
    }

    @Override
    public void consume(SelectedBudgetContract.Action action) {
        if (action instanceof SelectedBudgetContract.Action.FetchSignedInUserName) {
            fetchSignedInUserName();
        } else if (action instanceof SelectedBudgetContract.Action.FetchBudgetName) {
            fetchBudgetName(((SelectedBudgetContract.Action.FetchBudgetName) action).getIdBudget());
        }
    }

    private void fetchSignedInUserName() {
        send(new SelectedBudgetContract.Event.RenderSignedInUserName(signedUser.getName()));
    }

    private void fetchBudgetName(String idBudget) {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    for (Budget budget : budgets) {
                        if (budget.getId().equals(idBudget)) {
                            send(new SelectedBudgetContract.Event.RenderBudgetName(budget.getName()));
                            break;
                        }
                    }
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch budget name", error) ;
                }));
    }
}