package com.katarzynaiwonakrzemien.domain.contract.budget.spending;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BasePresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BudgetSpendingPresenter extends BasePresenter<BudgetSpendingContract.Event, BudgetSpendingContract.Action> implements BudgetSpendingContract.Presenter {

    private final RepositoryManager repository;
    private final User signedInUser;
    private final ItemToUiTransformer<Spending, SpendingUi> spendingItemToUiTransformer;
    private final ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer;

    public BudgetSpendingPresenter(RxSchedulers schedulers, RepositoryManager repository, User signedInUser, ItemToUiTransformer<Spending, SpendingUi> spendingItemToUiTransformer, ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer) {
        super(schedulers);
        this.repository = repository;
        this.signedInUser = signedInUser;
        this.spendingItemToUiTransformer = spendingItemToUiTransformer;
        this.budgetItemToUiTransformer = budgetItemToUiTransformer;
    }

    @Override
    public void consume(BudgetSpendingContract.Action action) {
        if (action instanceof BudgetSpendingContract.Action.FetchSpending) {
            fetchSpending(((BudgetSpendingContract.Action.FetchSpending) action).getIdBudget());
        } else if (action instanceof BudgetSpendingContract.Action.FetchCurrentDebt) {
            fetchCurrentDebt(((BudgetSpendingContract.Action.FetchCurrentDebt) action).getIdBudget());
        } else if (action instanceof BudgetSpendingContract.Action.FetchSignInUserSpendingSum) {
            fetchSignInUserSpendingSum(((BudgetSpendingContract.Action.FetchSignInUserSpendingSum) action).getIdBudget());
        } else if (action instanceof BudgetSpendingContract.Action.AddSpending) {
            BudgetSpendingContract.Action.AddSpending actionAddSpending = (BudgetSpendingContract.Action.AddSpending) action;
            addSpending(actionAddSpending.getIdBudget(), actionAddSpending.getProduct(), actionAddSpending.getAmount(), actionAddSpending.getDate());
        } else if (action instanceof BudgetSpendingContract.Action.Refresh) {
            refresh();
        }
    }

    private void fetchSpending(String idBudget) {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    List<SpendingUi> spendingList = new ArrayList<>();
                    for (Budget budget : budgets) {
                        if (budget.getId().equals(idBudget)) {
                            for (Spending spending : budget.getSpending()) {
                                spendingList.add(spendingItemToUiTransformer.transform(spending));
                            }
                            break;
                        }
                    }
                    Collections.sort(spendingList, new SpendingUi.ProductComparator());
                    Collections.sort(spendingList, new SpendingUi.DayComparator());
                    Collections.sort(spendingList, new SpendingUi.MonthComparator());
                    Collections.sort(spendingList, new SpendingUi.YearComparator());
                    send(new BudgetSpendingContract.Event.RenderSpending(spendingList));
                }, error ->
                {
                    //TODO    Log.e(Log.getTAG(this), "Failure to fetch spending", error) ;
                }));
    }

    private void fetchCurrentDebt(String idBudget) {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    for (Budget budget : budgets) {
                        if (budget.getId().equals(idBudget)) {
                            send(new BudgetSpendingContract.Event.RenderCurrentDebt(budgetItemToUiTransformer.transform(budget).getDebt()));
                            send(new BudgetSpendingContract.Event.RefreshDone());
                            break;
                        }
                    }
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch current debt", error) ;
                }));
    }

    private void fetchSignInUserSpendingSum(String idBudget) {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    for (Budget budget : budgets) {
                        if (budget.getId().equals(idBudget)) {
                            double sum = 0.0;
                            for (Spending spending : budget.getSpending()) {
                                if (spending.getIdUser().equals(signedInUser.getId())) {
                                    sum += spending.getAmount();
                                }
                            }
                            for (Debt debt : budget.getRepaidDebts()) {
                                if (debt.getIdUser().equals(signedInUser.getId())) {
                                    if (debt.getStatus() == Debt.CONFIRMED || debt.getStatus() == Debt.WAITING) {
                                        sum += debt.getAmount();
                                    }
                                }
                            }
                            send(new BudgetSpendingContract.Event.RenderSignInUserSpendingSum(sum));
                            break;
                        }
                    }
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch sign in user spending sum", error) ;
                }));
    }


    private void addSpending(String idBudget, String product, double amount, String date) {
        repository.addSpending(new Spending(idBudget, signedInUser.getId(), signedInUser.getName(), product, amount, date));
    }

    private void refresh() {
        repository.refreshBudgets();
    }
}
