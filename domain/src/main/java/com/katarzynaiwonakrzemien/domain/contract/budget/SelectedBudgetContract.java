package com.katarzynaiwonakrzemien.domain.contract.budget;

import com.katarzynaiwonakrzemien.domain.BaseContract;

public class SelectedBudgetContract {

    public interface Presenter extends BaseContract.Presenter<Event, Action> {
    }

    public interface View extends BaseContract.View {
    }

    public static class Action implements BaseContract.Action {

        public static class FetchSignedInUserName extends Action {
        }

        public static class FetchBudgetName extends Action {
            private final String idBudget;

            public FetchBudgetName(String idBudget) {
                this.idBudget = idBudget;
            }

            public String getIdBudget() {
                return idBudget;
            }
        }
    }

    public static class Event implements BaseContract.Event {

        public static class RenderSignedInUserName extends Event {
            private final String userName;

            public RenderSignedInUserName(String userName) {
                this.userName = userName;
            }

            public String getUserName() {
                return userName;
            }
        }

        public static class RenderBudgetName extends Event {
            private final String budgetName;

            public RenderBudgetName(String budgetName) {
                this.budgetName = budgetName;
            }

            public String getBudgetName() {
                return budgetName;
            }
        }
    }
}
