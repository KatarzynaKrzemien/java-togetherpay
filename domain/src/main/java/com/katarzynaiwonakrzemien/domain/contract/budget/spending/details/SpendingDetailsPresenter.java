package com.katarzynaiwonakrzemien.domain.contract.budget.spending.details;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BasePresenter;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;

import java.util.HashMap;
import java.util.Map;

public class SpendingDetailsPresenter extends BasePresenter<SpendingDetailsContract.Event, SpendingDetailsContract.Action> implements SpendingDetailsContract.Presenter {

    private final RepositoryManager repository;
    private final ItemToUiTransformer<Spending, SpendingUi> spendingItemToUiTransformer;

    public SpendingDetailsPresenter(RxSchedulers schedulers, RepositoryManager repository, ItemToUiTransformer<Spending, SpendingUi> spendingItemToUiTransformer) {
        super(schedulers);
        this.repository = repository;
        this.spendingItemToUiTransformer = spendingItemToUiTransformer;
    }

    @Override
    public void consume(SpendingDetailsContract.Action action) {
        if (action instanceof SpendingDetailsContract.Action.FetchSpending) {
            SpendingDetailsContract.Action.FetchSpending actionSpending = (SpendingDetailsContract.Action.FetchSpending) action;
            fetchSpending(actionSpending.getIdBudget(), actionSpending.getIdSpending());
        } else if (action instanceof SpendingDetailsContract.Action.FetchSelectedBudgetUsers) {
            fetchSelectedBudgetUsers(((SpendingDetailsContract.Action.FetchSelectedBudgetUsers) action).getIdBudget());
        } else if (action instanceof SpendingDetailsContract.Action.DeleteSpending) {
            deleteSpending(((SpendingDetailsContract.Action.DeleteSpending) action).getIdSpending());
        } else if (action instanceof SpendingDetailsContract.Action.UpdateSpending) {
            SpendingDetailsContract.Action.UpdateSpending actionUpdateSpending = (SpendingDetailsContract.Action.UpdateSpending) action;
            updateSpending(
                    actionUpdateSpending.getIdBudget(),
                    actionUpdateSpending.getIdSpending(),
                    actionUpdateSpending.getUserId(),
                    actionUpdateSpending.getUserName(),
                    actionUpdateSpending.getProduct(),
                    actionUpdateSpending.getDate(),
                    actionUpdateSpending.getAmount()
            );
        }
    }

    private void fetchSpending(String idBudget, String idSpending) {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    for (Budget budget : budgets) {
                        if (budget.getId().equals(idBudget)) {
                            for (Spending spending : budget.getSpending()) {
                                if (spending.getId().equals(idSpending)) {
                                    send(new SpendingDetailsContract.Event.RenderSpending(
                                                    spendingItemToUiTransformer.transform(spending)
                                            )
                                    );
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }, error ->
                {
                    //TODO    Log.e(Log.getTAG(this), "Failure to fetch spending", error) ;
                }));
    }

    private void fetchSelectedBudgetUsers(String idBudget) {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    for (Budget budget : budgets) {
                        if (budget.getId().equals(idBudget)) {
                            Map<String, String> users = new HashMap<>();
                            users.put(budget.getFirstUserName(), budget.getFirstUserId());
                            users.put(budget.getSecondUserName(), budget.getSecondUserId());
                            send(new SpendingDetailsContract.Event.RenderSelectedBudgetUsers(users));
                            break;
                        }
                    }
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch selected budget users", error) ;
                }));
    }

    private void deleteSpending(String idSpending) {
        repository.deleteSpending(idSpending);
    }

    private void updateSpending(String idBudget, String idSpending, String userId, String userName, String product, String date, double amount) {
        repository.updateSpending(new Spending(idSpending, idBudget, userId, userName, product, amount, date));
    }
}
