package com.katarzynaiwonakrzemien.domain.contract.budget.spending.details;

import com.katarzynaiwonakrzemien.domain.BaseContract;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;

import java.util.Map;

public class SpendingDetailsContract {
    public interface Presenter extends BaseContract.Presenter<Event, Action> {
    }

    public interface View extends BaseContract.View {
    }

    public static class Action implements BaseContract.Action {

        public static class FetchSpending extends Action {
            private final String idBudget;
            private final String idSpending;

            public FetchSpending(String idBudget, String idSpending) {
                this.idBudget = idBudget;
                this.idSpending = idSpending;
            }

            public String getIdBudget() {
                return idBudget;
            }

            public String getIdSpending() {
                return idSpending;
            }
        }

        public static class FetchSelectedBudgetUsers extends Action {
            private final String idBudget;

            public FetchSelectedBudgetUsers(String idBudget) {
                this.idBudget = idBudget;
            }

            public String getIdBudget() {
                return idBudget;
            }
        }

        public static class DeleteSpending extends Action {
            private final String idSpending;

            public DeleteSpending(String idSpending) {
                this.idSpending = idSpending;
            }

            public String getIdSpending() {
                return idSpending;
            }
        }

        public static class UpdateSpending extends Action {
            private final String idBudget;
            private final String idSpending;
            private final String userId;
            private final String userName;
            private final String product;
            private final String date;
            private final double amount;

            public UpdateSpending(String idBudget, String idSpending, String userId, String userName, String product, String date, double amount) {
                this.idBudget = idBudget;
                this.idSpending = idSpending;
                this.userId = userId;
                this.userName = userName;
                this.product = product;
                this.date = date;
                this.amount = amount;
            }

            public String getIdBudget() {
                return idBudget;
            }

            public String getIdSpending() {
                return idSpending;
            }

            public String getUserId() {
                return userId;
            }

            public String getUserName() {
                return userName;
            }

            public String getProduct() {
                return product;
            }

            public String getDate() {
                return date;
            }

            public double getAmount() {
                return amount;
            }
        }
    }

    public static class Event implements BaseContract.Event {

        public static class RenderSpending extends Event {
            private final SpendingUi spending;

            public RenderSpending(SpendingUi spending) {
                this.spending = spending;
            }

            public SpendingUi getSpending() {
                return spending;
            }
        }

        public static class RenderSelectedBudgetUsers extends Event {
            private final Map<String, String> users;

            public RenderSelectedBudgetUsers(Map<String, String> users) {
                this.users = users;
            }

            public Map<String, String> getUsers() {
                return users;
            }
        }
    }
}