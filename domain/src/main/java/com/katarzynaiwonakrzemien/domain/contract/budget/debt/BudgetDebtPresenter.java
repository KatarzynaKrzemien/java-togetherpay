package com.katarzynaiwonakrzemien.domain.contract.budget.debt;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BasePresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.DateUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BudgetDebtPresenter extends BasePresenter<BudgetDebtContract.Event, BudgetDebtContract.Action> implements BudgetDebtContract.Presenter {

    private final RepositoryManager repository;
    private final User signedInUser;
    private final ItemToUiTransformer<Debt, DebtUi> debtItemToUiTransformer;
    private final ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer;
    private final UiToItemTransformer<DebtUi, Debt> debtUiToItemTransformer;

    public BudgetDebtPresenter(RxSchedulers schedulers, RepositoryManager repository, User signedInUser, ItemToUiTransformer<Debt, DebtUi> debtItemToUiTransformer, ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer, UiToItemTransformer<DebtUi, Debt> debtUiToItemTransformer) {
        super(schedulers);
        this.repository = repository;
        this.signedInUser = signedInUser;
        this.debtItemToUiTransformer = debtItemToUiTransformer;
        this.budgetItemToUiTransformer = budgetItemToUiTransformer;
        this.debtUiToItemTransformer = debtUiToItemTransformer;
    }

    @Override
    public void consume(BudgetDebtContract.Action action) {
        if (action instanceof BudgetDebtContract.Action.FetchRepaidDebts) {
            fetchRepaidDebts(((BudgetDebtContract.Action.FetchRepaidDebts) action).getIdBudget());
        } else if (action instanceof BudgetDebtContract.Action.FetchCurrentDebt) {
            fetchCurrentDebt(((BudgetDebtContract.Action.FetchCurrentDebt) action).getIdBudget());
        } else if (action instanceof BudgetDebtContract.Action.RejectDebt) {
            BudgetDebtContract.Action.RejectDebt actionReject = (BudgetDebtContract.Action.RejectDebt) action;
            updateDebt(actionReject.getDebt(), actionReject.getStatus(), actionReject.getUpdateDate());
        } else if (action instanceof BudgetDebtContract.Action.ConfirmDebt) {
            BudgetDebtContract.Action.ConfirmDebt actionConfirm = (BudgetDebtContract.Action.ConfirmDebt) action;
            updateDebt(actionConfirm.getDebt(), actionConfirm.getStatus(), actionConfirm.getUpdateDate());
        } else if (action instanceof BudgetDebtContract.Action.AddDebt) {
            addDebt(((BudgetDebtContract.Action.AddDebt) action).getIdBudget(), ((BudgetDebtContract.Action.AddDebt) action).getAmount(), ((BudgetDebtContract.Action.AddDebt) action).getDate());
        } else if (action instanceof BudgetDebtContract.Action.Refresh) {
            refresh();
        }
    }

    private void fetchRepaidDebts(String idBudget) {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    List<DebtUi> repaidDebts = new ArrayList<>();
                    for (Budget budget : budgets) {
                        if (budget.getId().equals(idBudget)) {
                            for (Debt debt : budget.getRepaidDebts()) {
                                repaidDebts.add(debtItemToUiTransformer.transform(debt));
                            }
                            break;
                        }
                    }
                    Collections.sort(repaidDebts, new DebtUi.DayComparator());
                    Collections.sort(repaidDebts, new DebtUi.MonthComparator());
                    Collections.sort(repaidDebts, new DebtUi.YearComparator());
                    Collections.sort(repaidDebts, new DebtUi.DayUpdateComparator());
                    Collections.sort(repaidDebts, new DebtUi.MonthUpdateComparator());
                    Collections.sort(repaidDebts, new DebtUi.YearUpdateComparator());
                    send(new BudgetDebtContract.Event.RenderRepaidDebts(repaidDebts));
                    send(new BudgetDebtContract.Event.RefreshDone());
                }, error ->
                {
                    //TODO    Log.e(Log.getTAG(this), "Failure to fetch repaid debts", error) ;
                }));
    }

    private void fetchCurrentDebt(String idBudget) {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    for (Budget budget : budgets) {
                        if (budget.getId().equals(idBudget)) {
                            send(new BudgetDebtContract.Event.RenderCurrentDebt(budgetItemToUiTransformer.transform(budget).getDebt()));
                            send(new BudgetDebtContract.Event.RefreshDone());
                            break;
                        }
                    }
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch current debt", error) ;
                }));
    }

    private void updateDebt(DebtUi debt, DebtUi.Status status, DateUi updateDate) {
        repository.updateDebt(debtUiToItemTransformer.transform(
                new DebtUi(
                        debt.getId(),
                        debt.getUser(),
                        debt.getIdBudget(),
                        status,
                        updateDate,
                        debt.getAmount(),
                        debt.getDate()
                ))
        );
    }

    private void addDebt(String idBudget, double amount, String date) {
        repository.addDebt(new Debt(idBudget, signedInUser.getId(), signedInUser.getName(), date, amount, date));
    }

    private void refresh() {
        repository.refreshBudgets();
    }
}
