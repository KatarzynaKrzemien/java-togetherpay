package com.katarzynaiwonakrzemien.domain.contract.start;

import com.katarzynaiwonakrzemien.domain.BaseContract;

public class StartContract {

    public interface Presenter extends BaseContract.Presenter<Event, Action> {
    }

    public interface View extends BaseContract.View {
    }

    public static class Action implements BaseContract.Action {

        public static class AddSignedInUser extends Action {
            private final String id;
            private final String name;

            public AddSignedInUser(String id, String name) {
                this.id = id;
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public String getName() {
                return name;
            }
        }

        public static class SignedOutUser extends Action {
        }
    }

    public static class Event implements BaseContract.Event {
    }
}