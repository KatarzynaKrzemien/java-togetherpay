package com.katarzynaiwonakrzemien.domain.contract.budget.details;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BasePresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;

public class BudgetDetailsPresenter extends BasePresenter<BudgetDetailsContract.Event, BudgetDetailsContract.Action> implements BudgetDetailsContract.Presenter {

    private final RepositoryManager repository;
    private final UiToItemTransformer<BudgetUi, Budget> budgetUiToItemTransformer;
    private final ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer;

    public BudgetDetailsPresenter(RxSchedulers schedulers, RepositoryManager repository, UiToItemTransformer<BudgetUi, Budget> budgetUiToItemTransformer, ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer) {
        super(schedulers);
        this.repository = repository;
        this.budgetUiToItemTransformer = budgetUiToItemTransformer;
        this.budgetItemToUiTransformer = budgetItemToUiTransformer;
    }


    @Override
    public void consume(BudgetDetailsContract.Action action) {
        if (action instanceof BudgetDetailsContract.Action.FetchBudget) {
            fetchBudget(((BudgetDetailsContract.Action.FetchBudget) action).getIdBudget());
        } else if (action instanceof BudgetDetailsContract.Action.UpdateBudget) {
            updateBudget(((BudgetDetailsContract.Action.UpdateBudget) action).getName(), ((BudgetDetailsContract.Action.UpdateBudget) action).getBudget());
        } else if (action instanceof BudgetDetailsContract.Action.DeleteBudget) {
            deleteBudget(((BudgetDetailsContract.Action.DeleteBudget) action).getBudget());
        }
    }

    private void fetchBudget(String idBudget) {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    for (Budget budget : budgets) {
                        if (budget.getId().equals(idBudget)) {
                            send(new BudgetDetailsContract.Event.RenderBudget(budgetItemToUiTransformer.transform(budget)));
                            break;
                        }
                    }
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch budget", error) ;
                }));
    }

    private void updateBudget(String name, BudgetUi budget) {
        repository.updateBudget(budgetUiToItemTransformer.transform(
                new BudgetUi(
                        budget.getId(),
                        name,
                        budget.getOtherUser(),
                        budget.getSpending(),
                        budget.getDebt(),
                        budget.getSpendingList(),
                        budget.getRepaidDebts()
                ))
        );
    }

    private void deleteBudget(BudgetUi budget) {
        repository.deleteBudget(budgetUiToItemTransformer.transform(budget));
    }
}