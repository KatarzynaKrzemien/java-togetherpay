package com.katarzynaiwonakrzemien.domain.contract.user.budget;

import com.katarzynaiwonakrzemien.domain.BaseContract;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;

import java.util.List;

public class BudgetSelectContract {

    public interface Presenter extends BaseContract.Presenter<Event, Action> {
    }

    public interface View extends BaseContract.View {
    }

    public static class Action implements BaseContract.Action {

        public static class FetchBudgets extends Action {
        }

        public static class FetchUsers extends Action {
        }

        public static class FetchDebtSum extends Action {
        }

        public static class FetchOwingSum extends Action {
        }

        public static class FetchSignInUserSpendingSum extends Action {
        }

        public static class Refresh extends Action {
        }

        public static class AddBudget extends Action {
            private final String name;
            private final UserUi idOtherUser;

            public AddBudget(String name, UserUi idOtherUser) {
                this.name = name;
                this.idOtherUser = idOtherUser;
            }

            public String getName() {
                return name;
            }

            public UserUi getIdOtherUser() {
                return idOtherUser;
            }
        }
    }

    public static class Event implements BaseContract.Event {

        public static class RenderBudgets extends Event {
            private final List<BudgetUi> budgets;

            public RenderBudgets(List<BudgetUi> budgets) {
                this.budgets = budgets;
            }

            public List<BudgetUi> getBudgets() {
                return budgets;
            }
        }

        public static class RenderDebtSum extends Event {
            private final Double amount;

            public RenderDebtSum(Double amount) {
                this.amount = amount;
            }

            public Double getAmount() {
                return amount;
            }
        }

        public static class RenderOwingSum extends Event {
            private final Double amount;

            public RenderOwingSum(Double amount) {
                this.amount = amount;
            }

            public Double getAmount() {
                return amount;
            }
        }

        public static class RenderSignInUserSpendingSum extends Event {
            private final Double amount;

            public RenderSignInUserSpendingSum(Double amount) {
                this.amount = amount;
            }

            public Double getAmount() {
                return amount;
            }
        }

        public static class RenderUsers extends Event {
            private final List<UserUi> users;

            public RenderUsers(List<UserUi> users) {
                this.users = users;
            }

            public List<UserUi> getUsers() {
                return users;
            }
        }

        public static class RefreshDone extends Event {
        }
    }
}