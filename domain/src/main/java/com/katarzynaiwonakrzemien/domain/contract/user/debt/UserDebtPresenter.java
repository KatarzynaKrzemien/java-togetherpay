package com.katarzynaiwonakrzemien.domain.contract.user.debt;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BasePresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.DateUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserDebtPresenter extends BasePresenter<UserDebtContract.Event, UserDebtContract.Action> implements UserDebtContract.Presenter {

    private final RepositoryManager repository;
    private final User signedInUser;
    private final ItemToUiTransformer<Debt, DebtUi> debtItemToUiTransformer;
    private final ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer;
    private final UiToItemTransformer<DebtUi, Debt> debtUiToItemTransformer;

    public UserDebtPresenter(RxSchedulers schedulers, RepositoryManager repository, User signedInUser, ItemToUiTransformer<Debt, DebtUi> debtItemToUiTransformer, ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer, UiToItemTransformer<DebtUi, Debt> debtUiToItemTransformer) {
        super(schedulers);
        this.repository = repository;
        this.signedInUser = signedInUser;
        this.debtItemToUiTransformer = debtItemToUiTransformer;
        this.budgetItemToUiTransformer = budgetItemToUiTransformer;
        this.debtUiToItemTransformer = debtUiToItemTransformer;
    }

    @Override
    public void consume(UserDebtContract.Action action) {
        if (action instanceof UserDebtContract.Action.FetchDebts) {
            fetchDebts();
        } else if (action instanceof UserDebtContract.Action.FetchDebtSum) {
            fetchDebtSum();
        } else if (action instanceof UserDebtContract.Action.FetchOwingSum) {
            fetchOwingSum();
        } else if (action instanceof UserDebtContract.Action.RejectDebt) {
            UserDebtContract.Action.RejectDebt actionReject = (UserDebtContract.Action.RejectDebt) action;
            updateDebt(actionReject.getDebt(), actionReject.getStatus(), actionReject.getUpdateDate());
        } else if (action instanceof UserDebtContract.Action.ConfirmDebt) {
            UserDebtContract.Action.ConfirmDebt actionConfirm = (UserDebtContract.Action.ConfirmDebt) action;
            updateDebt(actionConfirm.getDebt(), actionConfirm.getStatus(), actionConfirm.getUpdateDate());
        } else if (action instanceof UserDebtContract.Action.AddDebt) {
            UserDebtContract.Action.AddDebt actionAddDebt = (UserDebtContract.Action.AddDebt) action;
            addDebt(actionAddDebt.getIdBudget(), actionAddDebt.getAmount(), actionAddDebt.getDate());
        } else if (action instanceof UserDebtContract.Action.Refresh) {
            refresh();
        }
    }

    private void fetchDebts() {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    List<DebtUi> allUserDebtList = new ArrayList<>();
                    for (Budget budget : budgets) {
                        BudgetUi budgetUi = budgetItemToUiTransformer.transform(budget);
                        if (budgetUi.getDebt().getAmount() != 0.0) {
                            UserUi user;
                            if (budgetUi.getDebt().getBelongToSignInUser()) {
                                user = new UserUi(signedInUser.getId(), signedInUser.getName());
                            } else {
                                user = budgetUi.getOtherUser();

                            }
                            allUserDebtList.add(debtItemToUiTransformer.transform(
                                    new Debt(
                                            budgetUi.getId(),
                                            user.getId(),
                                            user.getName(),
                                            Debt.NOREPAY,
                                            budgetUi.getDebt().getAmount()
                                    )));
                        }
                        allUserDebtList.addAll(budgetUi.getRepaidDebts());
                    }
                    Collections.sort(allUserDebtList, new DebtUi.DayComparator());
                    Collections.sort(allUserDebtList, new DebtUi.MonthComparator());
                    Collections.sort(allUserDebtList, new DebtUi.YearComparator());
                    Collections.sort(allUserDebtList, new DebtUi.DayUpdateComparator());
                    Collections.sort(allUserDebtList, new DebtUi.MonthUpdateComparator());
                    Collections.sort(allUserDebtList, new DebtUi.YearUpdateComparator());
                    send(new UserDebtContract.Event.RenderDebts(allUserDebtList));
                    send(new UserDebtContract.Event.RefreshDone());
                }, error ->
                {
                    //TODO    Log.e(Log.getTAG(this), "Failure to fetch debts", error) ;
                }));
    }

    private void fetchDebtSum() {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    double sum = 0.0;
                    for (Budget budget : budgets) {
                        BudgetUi budgetUi = budgetItemToUiTransformer.transform(budget);
                        if (budgetUi.getDebt().getBelongToSignInUser()) {
                            sum += budgetUi.getDebt().getAmount();
                        }
                    }
                    send(new UserDebtContract.Event.RenderDebtSum(sum));
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch debt sum", error) ;
                }));
    }

    private void fetchOwingSum() {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    double sum = 0.0;
                    for (Budget budget : budgets) {
                        BudgetUi budgetUi = budgetItemToUiTransformer.transform(budget);
                        if (!budgetUi.getDebt().getBelongToSignInUser()) {
                            sum += budgetUi.getDebt().getAmount();
                        }
                    }
                    send(new UserDebtContract.Event.RenderOwingSum(sum));
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch owing sum", error) ;
                }));
    }

    private void updateDebt(DebtUi debt, DebtUi.Status status, DateUi updateDate) {
        repository.updateDebt(debtUiToItemTransformer.transform(
                new DebtUi(
                        debt.getId(),
                        debt.getUser(),
                        debt.getIdBudget(),
                        status,
                        updateDate,
                        debt.getAmount(),
                        debt.getDate()
                ))
        );
    }

    private void addDebt(String idBudget, double amount, String date) {
        repository.addDebt(new Debt(idBudget, signedInUser.getId(), signedInUser.getName(), date, amount, date));
    }

    private void refresh() {
        repository.refreshBudgets();
    }
}