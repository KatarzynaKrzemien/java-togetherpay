package com.katarzynaiwonakrzemien.domain.contract.start;

import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BasePresenter;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;

public class StartPresenter extends BasePresenter<StartContract.Event, StartContract.Action> implements StartContract.Presenter {

    private final RepositoryManager repository;
    private final SignedInUser signedInUser;

    public StartPresenter(RxSchedulers schedulers, RepositoryManager repository, SignedInUser signedInUser) {
        super(schedulers);
        this.repository = repository;
        this.signedInUser = signedInUser;
    }

    @Override
    public void consume(StartContract.Action action) {
        if (action instanceof StartContract.Action.AddSignedInUser) {
            StartContract.Action.AddSignedInUser actionSignInUser = (StartContract.Action.AddSignedInUser) action;
            addSignedInUser(actionSignInUser.getId(), actionSignInUser.getName());
        } else if (action instanceof StartContract.Action.SignedOutUser) {
            signedOutUser();
        }
    }

    private void addSignedInUser(String id, String name) {
        signedInUser.signIn(new User(id, name));
        repository.addUserIfNotExist(signedInUser.getUser());
    }

    private void signedOutUser() {
        repository.cleanData();
        signedInUser.signOut();
    }
}
