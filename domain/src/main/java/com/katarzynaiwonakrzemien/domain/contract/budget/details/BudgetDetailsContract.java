package com.katarzynaiwonakrzemien.domain.contract.budget.details;

import com.katarzynaiwonakrzemien.domain.BaseContract;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;

public class BudgetDetailsContract {

    public interface Presenter extends BaseContract.Presenter<Event, Action> {
    }

    public interface View extends BaseContract.View {
    }

    public static class Action implements BaseContract.Action {

        public static class FetchBudget extends Action {
            private final String idBudget;

            public FetchBudget(String idBudget) {
                this.idBudget = idBudget;
            }

            public String getIdBudget() {
                return idBudget;
            }
        }

        public static class DeleteBudget extends Action {
            private final BudgetUi budget;

            public DeleteBudget(BudgetUi budget) {
                this.budget = budget;
            }

            public BudgetUi getBudget() {
                return budget;
            }
        }

        public static class UpdateBudget extends Action {
            private final String name;
            private final BudgetUi budget;

            public UpdateBudget(String name, BudgetUi budget) {
                this.name = name;
                this.budget = budget;
            }

            public String getName() {
                return name;
            }

            public BudgetUi getBudget() {
                return budget;
            }
        }
    }

    public static class Event implements BaseContract.Event {

        public static class RenderBudget extends Event {
            private final BudgetUi budget;

            public RenderBudget(BudgetUi budget) {
                this.budget = budget;
            }

            public BudgetUi getBudget() {
                return budget;
            }
        }
    }
}
