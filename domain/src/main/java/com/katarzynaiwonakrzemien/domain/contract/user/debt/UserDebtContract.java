package com.katarzynaiwonakrzemien.domain.contract.user.debt;

import com.katarzynaiwonakrzemien.domain.BaseContract;
import com.katarzynaiwonakrzemien.domain.model.DateUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;

import java.util.Calendar;
import java.util.List;

public class UserDebtContract {

    public interface Presenter extends BaseContract.Presenter<Event, Action> {
    }

    public interface View extends BaseContract.View {
    }

    public static class Action implements BaseContract.Action {

        public static class FetchDebts extends Action {
        }

        public static class FetchDebtSum extends Action {
        }

        public static class FetchOwingSum extends Action {
        }

        public static class Refresh extends Action {
        }

        public static class RejectDebt extends Action {
            private final DebtUi debt;
            private final DateUi updateDate;
            private final DebtUi.Status status = DebtUi.Status.REJECTED;

            public RejectDebt(DebtUi debt, DateUi updateDate) {
                this.debt = debt;
                this.updateDate = updateDate;
            }

            public RejectDebt(DebtUi debt) {
                this.debt = debt;
                this.updateDate = new DateUi(
                        DateUi.createDate(
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.YEAR)
                        )
                );
            }

            public DebtUi getDebt() {
                return debt;
            }

            public DateUi getUpdateDate() {
                return updateDate;
            }

            public DebtUi.Status getStatus() {
                return status;
            }
        }

        public static class ConfirmDebt extends Action {
            private final DebtUi debt;
            private final DateUi updateDate;
            private final DebtUi.Status status = DebtUi.Status.CONFIRMED;

            public ConfirmDebt(DebtUi debt, DateUi updateDate) {
                this.debt = debt;
                this.updateDate = updateDate;
            }

            public ConfirmDebt(DebtUi debt) {
                this.debt = debt;
                this.updateDate = new DateUi(
                        DateUi.createDate(
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.YEAR)
                        )
                );
            }

            public DebtUi getDebt() {
                return debt;
            }

            public DateUi getUpdateDate() {
                return updateDate;
            }

            public DebtUi.Status getStatus() {
                return status;
            }
        }

        public static class AddDebt extends Action {
            private final String idBudget;
            private final Double amount;
            private final String date;

            public AddDebt(String idBudget, Double amount, String date) {
                this.idBudget = idBudget;
                this.amount = amount;
                this.date = date;
            }

            public AddDebt(String idBudget, Double amount) {
                this.idBudget = idBudget;
                this.amount = amount;
                this.date = DateUi.createDate(
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.YEAR)
                );
            }

            public String getIdBudget() {
                return idBudget;
            }

            public Double getAmount() {
                return amount;
            }

            public String getDate() {
                return date;
            }
        }
    }

    public static class Event implements BaseContract.Event {

        public static class RenderDebts extends Event {
            private final List<DebtUi> debts;

            public RenderDebts(List<DebtUi> debts) {
                this.debts = debts;
            }

            public List<DebtUi> getDebts() {
                return debts;
            }
        }


        public static class RenderDebtSum extends Event {
            private final Double debt;

            public RenderDebtSum(Double debt) {
                this.debt = debt;
            }

            public Double getDebt() {
                return debt;
            }
        }

        public static class RenderOwingSum extends Event {
            private final Double owing;

            public RenderOwingSum(Double owing) {
                this.owing = owing;
            }

            public Double getOwing() {
                return owing;
            }
        }

        public static class RefreshDone extends Event {
        }
    }
}
