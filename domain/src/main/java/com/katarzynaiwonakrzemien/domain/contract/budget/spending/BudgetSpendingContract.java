package com.katarzynaiwonakrzemien.domain.contract.budget.spending;

import com.katarzynaiwonakrzemien.domain.BaseContract;
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi;
import com.katarzynaiwonakrzemien.domain.model.DateUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;

import java.util.Calendar;
import java.util.List;

public class BudgetSpendingContract {

    public interface Presenter extends BaseContract.Presenter<Event, Action> {
    }

    public interface View extends BaseContract.View {
    }

    public static class Action implements BaseContract.Action {

        public static class FetchSpending extends Action {
            private final String idBudget;

            public FetchSpending(String idBudget) {
                this.idBudget = idBudget;
            }

            public String getIdBudget() {
                return idBudget;
            }
        }

        public static class FetchCurrentDebt extends Action {
            private final String idBudget;

            public FetchCurrentDebt(String idBudget) {
                this.idBudget = idBudget;
            }

            public String getIdBudget() {
                return idBudget;
            }
        }

        public static class FetchSignInUserSpendingSum extends Action {
            private final String idBudget;

            public FetchSignInUserSpendingSum(String idBudget) {
                this.idBudget = idBudget;
            }

            public String getIdBudget() {
                return idBudget;
            }
        }

        public static class Refresh extends Action {
        }

        public static class AddSpending extends Action {
            private final String idBudget;
            private final String product;
            private final Double amount;
            private final String date;

            public AddSpending(String idBudget, String product, Double amount, String date) {
                this.idBudget = idBudget;
                this.product = product;
                this.amount = amount;
                this.date = date;
            }

            public AddSpending(String idBudget, String product, Double amount) {
                this.idBudget = idBudget;
                this.product = product;
                this.amount = amount;
                this.date = DateUi.createDate(
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.YEAR)
                );
            }

            public String getIdBudget() {
                return idBudget;
            }

            public String getProduct() {
                return product;
            }

            public Double getAmount() {
                return amount;
            }

            public String getDate() {
                return date;
            }
        }
    }

    public static class Event implements BaseContract.Event {

        public static class RenderSpending extends Event {
            private final List<SpendingUi> spending;

            public RenderSpending(List<SpendingUi> spending) {
                this.spending = spending;
            }

            public List<SpendingUi> getSpending() {
                return spending;
            }
        }

        public static class RenderSignInUserSpendingSum extends Event {
            private final Double amount;

            public RenderSignInUserSpendingSum(Double amount) {
                this.amount = amount;
            }

            public Double getAmount() {
                return amount;
            }
        }

        public static class RenderCurrentDebt extends Event {
            private final AmountInfoUi debtInfo;

            public RenderCurrentDebt(AmountInfoUi debtInfo) {
                this.debtInfo = debtInfo;
            }

            public AmountInfoUi getDebtInfo() {
                return debtInfo;
            }
        }

        public static class RefreshDone extends Event {
        }
    }
}