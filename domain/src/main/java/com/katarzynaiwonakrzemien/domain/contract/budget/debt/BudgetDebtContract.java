package com.katarzynaiwonakrzemien.domain.contract.budget.debt;

import com.katarzynaiwonakrzemien.domain.BaseContract;
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi;
import com.katarzynaiwonakrzemien.domain.model.DateUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;

import java.util.Calendar;
import java.util.List;

public class BudgetDebtContract {
    public interface Presenter extends BaseContract.Presenter<Event, Action> {
    }

    public interface View extends BaseContract.View {
    }

    public static class Action implements BaseContract.Action {

        public static class FetchRepaidDebts extends Action {
            private final String idBudget;

            public FetchRepaidDebts(String idBudget) {
                this.idBudget = idBudget;
            }

            public String getIdBudget() {
                return idBudget;
            }
        }

        public static class FetchCurrentDebt extends Action {
            private final String idBudget;

            public FetchCurrentDebt(String idBudget) {
                this.idBudget = idBudget;
            }

            public String getIdBudget() {
                return idBudget;
            }
        }

        public static class RejectDebt extends Action {
            private final DebtUi debt;
            private final DateUi updateDate;
            private final DebtUi.Status status = DebtUi.Status.REJECTED;

            public RejectDebt(DebtUi debt, DateUi updateDate) {
                this.debt = debt;
                this.updateDate = updateDate;
            }

            public RejectDebt(DebtUi debt) {
                this.debt = debt;
                this.updateDate = new DateUi(
                        DateUi.createDate(
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.YEAR)
                        )
                );
            }

            public DebtUi getDebt() {
                return debt;
            }

            public DateUi getUpdateDate() {
                return updateDate;
            }

            public DebtUi.Status getStatus() {
                return status;
            }
        }

        public static class ConfirmDebt extends Action {
            private final DebtUi debt;
            private final DateUi updateDate;
            private final DebtUi.Status status = DebtUi.Status.CONFIRMED;

            public ConfirmDebt(DebtUi debt, DateUi updateDate) {
                this.debt = debt;
                this.updateDate = updateDate;
            }

            public ConfirmDebt(DebtUi debt) {
                this.debt = debt;
                this.updateDate = new DateUi(
                        DateUi.createDate(
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.YEAR)
                        )
                );
            }

            public DebtUi getDebt() {
                return debt;
            }

            public DateUi getUpdateDate() {
                return updateDate;
            }

            public DebtUi.Status getStatus() {
                return status;
            }
        }

        public static class Refresh extends Action {
        }

        public static class AddDebt extends Action {
            private final String idBudget;
            private final Double amount;
            private final String date;

            public AddDebt(String idBudget, Double amount, String date) {
                this.idBudget = idBudget;
                this.amount = amount;
                this.date = date;
            }

            public AddDebt(String idBudget, Double amount) {
                this.idBudget = idBudget;
                this.amount = amount;
                this.date = DateUi.createDate(
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.YEAR)
                );
            }

            public String getIdBudget() {
                return idBudget;
            }

            public Double getAmount() {
                return amount;
            }

            public String getDate() {
                return date;
            }
        }
    }

    public static class Event implements BaseContract.Event {

        public static class RenderRepaidDebts extends Event {
            private final List<DebtUi> debts;

            public RenderRepaidDebts(List<DebtUi> debts) {
                this.debts = debts;
            }

            public List<DebtUi> getDebts() {
                return debts;
            }
        }

        public static class RenderCurrentDebt extends Event {
            private final AmountInfoUi debtInfo;

            public RenderCurrentDebt(AmountInfoUi debtInfo) {
                this.debtInfo = debtInfo;
            }

            public AmountInfoUi getDebtInfo() {
                return debtInfo;
            }
        }

        public static class RefreshDone extends Event {
        }
    }
}
