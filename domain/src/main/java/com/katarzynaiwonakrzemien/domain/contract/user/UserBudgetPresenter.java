package com.katarzynaiwonakrzemien.domain.contract.user;

import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BasePresenter;

public class UserBudgetPresenter extends BasePresenter<UserBudgetContract.Event, UserBudgetContract.Action> implements UserBudgetContract.Presenter {

    private final User signedUser;

    public UserBudgetPresenter(RxSchedulers schedulers, User signedUser) {
        super(schedulers);
        this.signedUser = signedUser;
    }

    @Override
    public void consume(UserBudgetContract.Action action) {
        if (action instanceof UserBudgetContract.Action.FetchSignedInUserName) {
            fetchSignedInUserName();
        }
    }

    private void fetchSignedInUserName() {
        send(new UserBudgetContract.Event.RenderSignedInUserName(signedUser.getName()));
    }
}
