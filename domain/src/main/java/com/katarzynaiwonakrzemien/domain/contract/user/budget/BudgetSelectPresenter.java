package com.katarzynaiwonakrzemien.domain.contract.user.budget;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BasePresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BudgetSelectPresenter extends BasePresenter<BudgetSelectContract.Event, BudgetSelectContract.Action> implements BudgetSelectContract.Presenter {

    private final RepositoryManager repository;
    private final User signedUser;
    private final ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer;
    private final ItemToUiTransformer<User, UserUi> userItemToUiTransformer;

    public BudgetSelectPresenter(RxSchedulers schedulers, RepositoryManager repository, User signedUser, ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer, ItemToUiTransformer<User, UserUi> userItemToUiTransformer) {
        super(schedulers);
        this.repository = repository;
        this.signedUser = signedUser;
        this.budgetItemToUiTransformer = budgetItemToUiTransformer;
        this.userItemToUiTransformer = userItemToUiTransformer;
    }

    @Override
    public void consume(BudgetSelectContract.Action action) {
        if (action instanceof BudgetSelectContract.Action.FetchBudgets) {
            fetchBudgets();
        } else if (action instanceof BudgetSelectContract.Action.FetchUsers) {
            fetchUsers();
        } else if (action instanceof BudgetSelectContract.Action.FetchDebtSum) {
            fetchDebtSum();
        } else if (action instanceof BudgetSelectContract.Action.FetchOwingSum) {
            fetchOwingSum();
        } else if (action instanceof BudgetSelectContract.Action.FetchSignInUserSpendingSum) {
            fetchSignInUserSpendingSum();
        } else if (action instanceof BudgetSelectContract.Action.AddBudget) {
            BudgetSelectContract.Action.AddBudget actionAddBudget = (BudgetSelectContract.Action.AddBudget) action;
            addBudget(actionAddBudget.getName(), actionAddBudget.getIdOtherUser());
        } else if (action instanceof BudgetSelectContract.Action.Refresh) {
            refresh();
        }
    }

    private void fetchBudgets() {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    List<BudgetUi> budgetUiList = new ArrayList<>();
                    for (Budget budget : budgets) {
                        budgetUiList.add(budgetItemToUiTransformer.transform(budget));
                    }
                    Collections.sort(budgetUiList, new BudgetUi.NameComparator());
                    send(new BudgetSelectContract.Event.RenderBudgets(budgetUiList));
                    send(new BudgetSelectContract.Event.RefreshDone());
                }, error ->
                {
                    //TODO    Log.e(Log.getTAG(this), "Failure to fetch budgets", error) ;
                }));
    }

    private void fetchUsers() {
        addDisposable(repository.users()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(users -> {
                    List<UserUi> usersUiList = new ArrayList<>();
                    for (User user : users) {
                        if (!user.getId().equals(signedUser.getId())) {
                            usersUiList.add(userItemToUiTransformer.transform(user));
                        }
                    }
                    Collections.sort(usersUiList, new UserUi.NameComparator());
                    send(new BudgetSelectContract.Event.RenderUsers(usersUiList));
                }, error ->
                {
                    //TODO    Log.e(Log.getTAG(this), "Failure to fetch users", error) ;
                }));
    }


    private void fetchDebtSum() {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    double sum = 0.0;
                    for (Budget budget : budgets) {
                        BudgetUi budgetUi = budgetItemToUiTransformer.transform(budget);
                        if (budgetUi.getDebt().getBelongToSignInUser()) {
                            sum += budgetUi.getDebt().getAmount();
                        }
                    }
                    send(new BudgetSelectContract.Event.RenderDebtSum(sum));
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch debt sum", error) ;
                }));
    }

    private void fetchOwingSum() {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    double sum = 0.0;
                    for (Budget budget : budgets) {
                        BudgetUi budgetUi = budgetItemToUiTransformer.transform(budget);
                        if (!budgetUi.getDebt().getBelongToSignInUser()) {
                            sum += budgetUi.getDebt().getAmount();
                        }
                    }
                    send(new BudgetSelectContract.Event.RenderOwingSum(sum));
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch owing sum", error) ;
                }));
    }

    private void fetchSignInUserSpendingSum() {
        addDisposable(repository.budgets()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(budgets -> {
                    double sum = 0.0;
                    for (Budget budget : budgets) {
                        for (Spending spending : budget.getSpending()) {
                            if (spending.getIdUser().equals(signedUser.getId())) {
                                sum += spending.getAmount();
                            }
                        }
                        for (Debt debt : budget.getRepaidDebts()) {
                            if (debt.getIdUser().equals(signedUser.getId())) {
                                if (debt.getStatus() == Debt.CONFIRMED || debt.getStatus() == Debt.WAITING) {
                                    sum += debt.getAmount();
                                }
                            }
                        }
                    }
                    send(new BudgetSelectContract.Event.RenderSignInUserSpendingSum(sum));
                }, error ->
                {
                    //TODO   Log.e(Log.getTAG(this), "Failure to fetch sign in user spending sum", error) ;
                }));
    }

    private void addBudget(String name, UserUi otherUser) {
        repository.addBudget(new Budget(name, signedUser.getId(), signedUser.getName(), otherUser.getId(), otherUser.getName()));
    }

    private void refresh() {
        repository.refreshBudgets();
        repository.refreshUsers();
    }
}
