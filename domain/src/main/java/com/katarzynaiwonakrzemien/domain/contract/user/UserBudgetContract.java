package com.katarzynaiwonakrzemien.domain.contract.user;

import com.katarzynaiwonakrzemien.domain.BaseContract;

public class UserBudgetContract {

    public interface Presenter extends BaseContract.Presenter<Event, Action> {
    }

    public interface View extends BaseContract.View {
    }

    public static class Action implements BaseContract.Action {

        public static class FetchSignedInUserName extends Action {
        }
    }

    public static class Event implements BaseContract.Event {

        public static class RenderSignedInUserName extends Event {
            private final String name;

            public RenderSignedInUserName(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }
        }
    }
}