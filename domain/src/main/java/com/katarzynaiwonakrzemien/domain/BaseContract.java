package com.katarzynaiwonakrzemien.domain;

import io.reactivex.Observable;

public class BaseContract {
    public interface Presenter<E extends Event, A extends Action> {

        void attach(Observable<A> view);

        Observable<E> subscribe();

        void dispose();
    }

    public interface View {
    }

    public interface Action {
    }

    public interface Event {
    }
}
