package com.katarzynaiwonakrzemien.domain.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateUi {

    private static final String splitRegex = "\\.";
    private static final String split = ".";
    private final String time;
    private final int year;
    private final int month;
    private final int day;

    public DateUi(String time) {
        this.time = time;
        String[] splitTime = time.split(splitRegex);
        if (!time.isEmpty()) {
            this.year = Integer.parseInt(splitTime[2]);
            this.month = Integer.parseInt(splitTime[1]) - 1;
            this.day = Integer.parseInt(splitTime[0]);
        } else {
            this.year = 0;
            this.month = 0;
            this.day = 0;
        }
    }

    public static String createDate(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return new SimpleDateFormat("dd" + split + "MM" + split + "YYYY", Locale.getDefault()).format(calendar.getTime());
    }

    public String getTime() {
        return time;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }
}
