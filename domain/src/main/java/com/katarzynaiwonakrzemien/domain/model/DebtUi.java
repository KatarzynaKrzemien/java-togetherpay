package com.katarzynaiwonakrzemien.domain.model;

import java.util.Comparator;

public class DebtUi implements ItemUi {

    private final String id;
    private final UserUi user;
    private final String idBudget;
    private final Status status;
    private final DateUi updateDate;
    private final AmountInfoUi amount;
    private final DateUi date;

    public DebtUi(String id, UserUi user, String idBudget, Status status, DateUi updateDate, AmountInfoUi amount, DateUi date) {
        this.id = id;
        this.user = user;
        this.idBudget = idBudget;
        this.status = status;
        this.updateDate = updateDate;
        this.amount = amount;
        this.date = date;
    }

    @Override
    public String getId() {
        return id;
    }

    public UserUi getUser() {
        return user;
    }

    public String getIdBudget() {
        return idBudget;
    }

    public Status getStatus() {
        return status;
    }

    public DateUi getUpdateDate() {
        return updateDate;
    }

    public AmountInfoUi getAmount() {
        return amount;
    }

    public DateUi getDate() {
        return date;
    }

    public enum Status {
        NOREPAY, CONFIRMED, WAITING, REJECTED
    }

    public static class DayUpdateComparator implements Comparator<DebtUi> {
        @Override
        public int compare(DebtUi debt1, DebtUi debt2) {
            return Integer.compare(debt2.getUpdateDate().getDay(), debt1.getUpdateDate().getDay());
        }
    }

    public static class MonthUpdateComparator implements Comparator<DebtUi> {
        @Override
        public int compare(DebtUi debt1, DebtUi debt2) {
            return Integer.compare(debt2.getUpdateDate().getMonth(), debt1.getUpdateDate().getMonth());
        }
    }

    public static class YearUpdateComparator implements Comparator<DebtUi> {
        @Override
        public int compare(DebtUi debt1, DebtUi debt2) {
            return Integer.compare(debt2.getUpdateDate().getDay(), debt1.getUpdateDate().getDay());
        }
    }

    public static class DayComparator implements Comparator<DebtUi> {
        @Override
        public int compare(DebtUi debt1, DebtUi debt2) {
            return Integer.compare(debt2.getDate().getDay(), debt1.getDate().getDay());
        }
    }

    public static class MonthComparator implements Comparator<DebtUi> {
        @Override
        public int compare(DebtUi debt1, DebtUi debt2) {
            return Integer.compare(debt2.getDate().getMonth(), debt1.getDate().getMonth());
        }
    }

    public static class YearComparator implements Comparator<DebtUi> {
        @Override
        public int compare(DebtUi debt1, DebtUi debt2) {
            return Integer.compare(debt2.getDate().getYear(), debt1.getDate().getYear());
        }
    }
}
