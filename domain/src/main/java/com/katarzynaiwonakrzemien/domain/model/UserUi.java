package com.katarzynaiwonakrzemien.domain.model;

import java.util.Comparator;

public class UserUi implements ItemUi {

    private final String id;
    private final String name;

    public UserUi(String id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }


    public static class NameComparator implements Comparator<UserUi> {
        @Override
        public int compare(UserUi user1, UserUi user2) {
            if (user1.getName() != null && user2.getName() != null) {
                return user1.getName().compareTo(user2.getName());
            } else if (user1.getName() == null && user2.getName() != null) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
