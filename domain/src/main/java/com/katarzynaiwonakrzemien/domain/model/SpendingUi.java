package com.katarzynaiwonakrzemien.domain.model;

import java.util.Comparator;

public class SpendingUi implements ItemUi {

    private final String id;
    private final String idBudget;
    private final UserUi user;
    private final String product;
    private final AmountInfoUi amount;
    private final DateUi date;

    public SpendingUi(String id, String idBudget, UserUi user, String product, AmountInfoUi amount, DateUi date) {
        this.id = id;
        this.idBudget = idBudget;
        this.user = user;
        this.product = product;
        this.amount = amount;
        this.date = date;
    }

    public String getIdBudget() {
        return idBudget;
    }

    public UserUi getUser() {
        return user;
    }

    public String getProduct() {
        return product;
    }

    public AmountInfoUi getAmount() {
        return amount;
    }

    public DateUi getDate() {
        return date;
    }

    @Override
    public String getId() {
        return id;
    }

    public static class ProductComparator implements Comparator<SpendingUi> {
        @Override
        public int compare(SpendingUi spending1, SpendingUi spending2) {
            if (spending1.getProduct() != null && spending2.getProduct() != null) {
                return spending1.getProduct().compareTo(spending2.getProduct());
            } else if (spending1.getProduct() == null && spending2.getProduct() != null) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public static class DayComparator implements Comparator<SpendingUi> {
        @Override
        public int compare(SpendingUi spending1, SpendingUi spending2) {
            return Integer.compare(spending2.getDate().getDay(), spending1.getDate().getDay());
        }
    }

    public static class MonthComparator implements Comparator<SpendingUi> {
        @Override
        public int compare(SpendingUi spending1, SpendingUi spending2) {
            return Integer.compare(spending2.getDate().getMonth(), spending1.getDate().getMonth());
        }
    }

    public static class YearComparator implements Comparator<SpendingUi> {
        @Override
        public int compare(SpendingUi spending1, SpendingUi spending2) {
            return Integer.compare(spending2.getDate().getYear(), spending1.getDate().getYear());
        }
    }
}
