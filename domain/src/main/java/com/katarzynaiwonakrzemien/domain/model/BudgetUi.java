package com.katarzynaiwonakrzemien.domain.model;

import java.util.Comparator;
import java.util.List;

public class BudgetUi implements ItemUi {

    private final String id;
    private final String name;
    private final UserUi otherUser;
    private final Double spending;
    private final AmountInfoUi debt;
    private final List<SpendingUi> spendingList;
    private final List<DebtUi> repaidDebts;

    public BudgetUi(String id, String name, UserUi otherUser, Double spending, AmountInfoUi debt, List<SpendingUi> spendingList, List<DebtUi> repaidDebts) {
        this.id = id;
        this.name = name;
        this.otherUser = otherUser;
        this.spending = spending;
        this.debt = debt;
        this.spendingList = spendingList;
        this.repaidDebts = repaidDebts;
    }

    public String getName() {
        return name;
    }

    public UserUi getOtherUser() {
        return otherUser;
    }

    public Double getSpending() {
        return spending;
    }

    public AmountInfoUi getDebt() {
        return debt;
    }

    public List<SpendingUi> getSpendingList() {
        return spendingList;
    }

    public List<DebtUi> getRepaidDebts() {
        return repaidDebts;
    }

    @Override
    public String getId() {
        return id;
    }

    public static class NameComparator implements Comparator<BudgetUi> {
        @Override
        public int compare(BudgetUi budget1, BudgetUi budget2) {
            if (budget1.getName() != null && budget2.getName() != null) {
                return budget1.getName().compareTo(budget2.getName());
            } else if (budget1.getName() == null && budget2.getName() != null) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
