package com.katarzynaiwonakrzemien.domain.model;

public class AmountInfoUi {

    private final Double amount;
    private final Boolean isBelongToSignInUser;

    public AmountInfoUi(Double amount, Boolean isBelongToSignInUser) {
        this.amount = amount;
        this.isBelongToSignInUser = isBelongToSignInUser;
    }

    public Double getAmount() {
        return amount;
    }

    public Boolean getBelongToSignInUser() {
        return isBelongToSignInUser;
    }
}
