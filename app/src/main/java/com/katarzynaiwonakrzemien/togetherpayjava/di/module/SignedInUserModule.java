package com.katarzynaiwonakrzemien.togetherpayjava.di.module;

import com.katarzynaiwonakrzemien.domain.user.SignedInUser;
import com.katarzynaiwonakrzemien.togetherpayjava.user.FirebaseSignedInUser;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SignedInUserModule {
    @Provides
    @Singleton
    public SignedInUser provideFirebaseSignedInUser() {
        return new FirebaseSignedInUser();
    }
}