package com.katarzynaiwonakrzemien.togetherpayjava.di.module.activity;

import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.contract.budget.SelectedBudgetContract;
import com.katarzynaiwonakrzemien.domain.contract.budget.SelectedBudgetPresenter;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;
import com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.SelectedBudgetScope;

import dagger.Module;
import dagger.Provides;

@Module
public class SelectedBudgetModule {
    @Provides
    @SelectedBudgetScope
    public SelectedBudgetContract.Presenter provideSelectedBudgetPresenter(
            RxSchedulers schedulers,
            RepositoryManager repository,
            SignedInUser signedUser
    ) {
        return new SelectedBudgetPresenter(schedulers, repository, signedUser.getUser());
    }

}
