package com.katarzynaiwonakrzemien.togetherpayjava.di.builder;

import com.katarzynaiwonakrzemien.togetherpayjava.di.module.activity.SelectedBudgetModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.activity.SpendingDetailsModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.activity.StartModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.activity.UserBudgetModule;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.SelectedBudgetActivity;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.spending.details.SpendingDetailsActivity;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.start.StartActivity;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.user.UserBudgetActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.SelectedBudgetScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.SpendingDetailsScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.StartScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.UserBudgetScope;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {SelectedBudgetModule.class})
    @SelectedBudgetScope
    public abstract SelectedBudgetActivity selectedBudgetActivity();

    @ContributesAndroidInjector(modules = {UserBudgetModule.class})
    @UserBudgetScope
    public abstract UserBudgetActivity userBudgetActivity();

    @ContributesAndroidInjector(modules = {StartModule.class})
    @StartScope
    public abstract StartActivity startActivity();

    @ContributesAndroidInjector(modules = {SpendingDetailsModule.class})
    @SpendingDetailsScope
    public abstract SpendingDetailsActivity spendingDetailsActivity();
}
