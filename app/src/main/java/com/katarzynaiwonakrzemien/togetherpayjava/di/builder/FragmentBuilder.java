package com.katarzynaiwonakrzemien.togetherpayjava.di.builder;

import com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment.BudgetDebtModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment.BudgetDetailsModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment.BudgetModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment.BudgetSelectModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment.UserDebtModule;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.debt.BudgetDebtFragment;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.details.BudgetDetailsFragment;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.spending.BudgetSpendingFragment;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.user.budgets.BudgetSelectFragment;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.user.debt.UserDebtFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.BudgetDebtScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.BudgetDetailsScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.BudgetScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.BudgetSelectScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.UserDebtScope;

@Module
public abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = {UserDebtModule.class})
    @UserDebtScope
    public abstract UserDebtFragment userDebtFragment();

    @ContributesAndroidInjector(modules = {BudgetDebtModule.class})
    @BudgetDebtScope
    public abstract BudgetDebtFragment budgetDebtFragment();

    @ContributesAndroidInjector(modules = {BudgetSelectModule.class})
    @BudgetSelectScope
    public abstract BudgetSelectFragment budgetSelectFragment();

    @ContributesAndroidInjector(modules = {BudgetModule.class})
    @BudgetScope
    public abstract BudgetSpendingFragment budgetFragment();

    @ContributesAndroidInjector(modules = {BudgetDetailsModule.class})
    @BudgetDetailsScope
    public abstract BudgetDetailsFragment budgetDetailsFragment();
}
