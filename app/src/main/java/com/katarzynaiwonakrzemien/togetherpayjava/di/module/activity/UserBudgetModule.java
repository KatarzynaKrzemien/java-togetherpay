package com.katarzynaiwonakrzemien.togetherpayjava.di.module.activity;

import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.contract.user.UserBudgetContract;
import com.katarzynaiwonakrzemien.domain.contract.user.UserBudgetPresenter;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;
import com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations;

import dagger.Module;
import dagger.Provides;

@Module
public class UserBudgetModule {
    @Provides
    @Annotations.UserBudgetScope
    public UserBudgetContract.Presenter provideUserBudgetPresenter(
            RxSchedulers schedulers, SignedInUser signedInUser
    ) {
        return new UserBudgetPresenter(schedulers, signedInUser.getUser());
    }
}