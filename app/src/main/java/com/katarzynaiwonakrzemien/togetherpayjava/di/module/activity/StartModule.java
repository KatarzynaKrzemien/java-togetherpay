package com.katarzynaiwonakrzemien.togetherpayjava.di.module.activity;

import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.contract.start.StartContract;
import com.katarzynaiwonakrzemien.domain.contract.start.StartPresenter;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;

import dagger.Module;
import dagger.Provides;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.StartScope;

@Module
public class StartModule {
    @Provides
    @StartScope
    public StartContract.Presenter provideStartPresenter(
            RxSchedulers schedulers,
            RepositoryManager repository,
            SignedInUser signedInUser
    ) {
        return new StartPresenter(schedulers, repository, signedInUser);
    }
}