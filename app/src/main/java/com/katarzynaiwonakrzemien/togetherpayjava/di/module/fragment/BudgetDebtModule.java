package com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.contract.budget.debt.BudgetDebtContract;
import com.katarzynaiwonakrzemien.domain.contract.budget.debt.BudgetDebtPresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;
import com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.BudgetDebtScope;
import com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.BudgetTransformer;

import dagger.Module;
import dagger.Provides;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.DebtTransformer;

@Module
public class BudgetDebtModule {
    @Provides
    @BudgetDebtScope
    public BudgetDebtContract.Presenter provideBudgetDebtPresenter(
            RxSchedulers schedulers, RepositoryManager repository, SignedInUser signedInUser,
            @DebtTransformer ItemToUiTransformer<Debt, DebtUi> debtItemToUiTransformer,
            @BudgetTransformer ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer,
            @DebtTransformer UiToItemTransformer<DebtUi, Debt> debtUiToItemTransformer
    ) {
        return new BudgetDebtPresenter(
                schedulers,
                repository,
                signedInUser.getUser(),
                debtItemToUiTransformer,
                budgetItemToUiTransformer,
                debtUiToItemTransformer
        );
    }
}