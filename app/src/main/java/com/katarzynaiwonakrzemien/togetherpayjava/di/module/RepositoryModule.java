package com.katarzynaiwonakrzemien.togetherpayjava.di.module;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.katarzynaiwonakrzemien.core.repository.Repository;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;
import com.katarzynaiwonakrzemien.togetherpayjava.repository.FirebaseRepository;
import com.katarzynaiwonakrzemien.togetherpayjava.repository.LocalRepositoryManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {
    @Provides
    @Singleton
    public Repository provideFirebaseRepository(SignedInUser signedInUser) {
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.setFirestoreSettings(new FirebaseFirestoreSettings.Builder().setPersistenceEnabled(false).build());
        return new FirebaseRepository(firebaseFirestore, signedInUser);
    }

    @Provides
    @Singleton
    public RepositoryManager provideRepositoryManager(Repository repository) {
        return new LocalRepositoryManager(repository);
    }
}
