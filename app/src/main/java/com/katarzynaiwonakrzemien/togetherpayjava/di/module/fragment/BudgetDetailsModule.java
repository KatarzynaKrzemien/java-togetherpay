package com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.contract.budget.details.BudgetDetailsContract;
import com.katarzynaiwonakrzemien.domain.contract.budget.details.BudgetDetailsPresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;
import com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers;

import dagger.Module;
import dagger.Provides;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.BudgetDetailsScope;

@Module
public class BudgetDetailsModule {
    @Provides
    @BudgetDetailsScope
    public BudgetDetailsContract.Presenter provideBudgetDetailsPresenter(
            RxSchedulers schedulers,
            RepositoryManager repository,
            @Qualifiers.BudgetTransformer ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer,
            @Qualifiers.BudgetTransformer UiToItemTransformer<BudgetUi, Budget> budgetUiToItemTransformer
    ) {
        return new BudgetDetailsPresenter(
                schedulers,
                repository,
                budgetUiToItemTransformer,
                budgetItemToUiTransformer
        );
    }
}