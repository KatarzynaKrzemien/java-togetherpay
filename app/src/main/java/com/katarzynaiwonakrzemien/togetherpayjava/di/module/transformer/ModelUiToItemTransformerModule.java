package com.katarzynaiwonakrzemien.togetherpayjava.di.module.transformer;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.budget.BudgetUiToItemTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.debt.DebtUiToItemTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.spending.SpendingUiToItemTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.user.UserUiToItemTransformer;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;

import dagger.Module;
import dagger.Provides;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.BudgetTransformer;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.DebtTransformer;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.SpendingTransformer;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.UserTransformer;

@Module
public class ModelUiToItemTransformerModule {
    @UserTransformer
    @Provides
    public UiToItemTransformer<UserUi, User> provideUserUiToItemTransformer() {
        return new UserUiToItemTransformer();
    }

    @DebtTransformer
    @Provides
    public UiToItemTransformer<DebtUi, Debt> provideDebtUiToItemTransformer() {
        return new DebtUiToItemTransformer();
    }

    @SpendingTransformer
    @Provides
    public UiToItemTransformer<SpendingUi, Spending> provideSpendingUiToItemTransformer() {
        return new SpendingUiToItemTransformer();
    }

    @BudgetTransformer
    @Provides
    public UiToItemTransformer<BudgetUi, Budget> provideBudgetUiToItemTransformer(
            SignedInUser signedInUser,
            @SpendingTransformer UiToItemTransformer<SpendingUi, Spending> spendingUiToItemTransformer,
            @DebtTransformer UiToItemTransformer<DebtUi, Debt> debtUiToItemTransformer
    ) {
        return new BudgetUiToItemTransformer(signedInUser.getUser(), spendingUiToItemTransformer, debtUiToItemTransformer);
    }
}