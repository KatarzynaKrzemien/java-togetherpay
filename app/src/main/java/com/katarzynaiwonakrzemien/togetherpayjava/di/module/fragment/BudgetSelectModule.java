package com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.contract.user.budget.BudgetSelectContract;
import com.katarzynaiwonakrzemien.domain.contract.user.budget.BudgetSelectPresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;

import dagger.Module;
import dagger.Provides;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.BudgetSelectScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.BudgetTransformer;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.UserTransformer;

@Module
public class BudgetSelectModule {
    @Provides
    @BudgetSelectScope
    public BudgetSelectContract.Presenter provideBudgetSelectPresenter(
            RxSchedulers schedulers,
            RepositoryManager repository,
            SignedInUser idSignedUser,
            @BudgetTransformer ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer,
            @UserTransformer ItemToUiTransformer<User, UserUi> userItemToUiTransformer
    ) {
        return new BudgetSelectPresenter(
                schedulers,
                repository,
                idSignedUser.getUser(),
                budgetItemToUiTransformer,
                userItemToUiTransformer
        );
    }
}