package com.katarzynaiwonakrzemien.togetherpayjava.di;

import javax.inject.Qualifier;


public class Qualifiers {
    @Qualifier
    public @interface UserTransformer {
    }

    @Qualifier
    public @interface BudgetTransformer {
    }

    @Qualifier
    public @interface DebtTransformer {
    }

    @Qualifier
    public @interface SpendingTransformer {
    }
}