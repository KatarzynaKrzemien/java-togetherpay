package com.katarzynaiwonakrzemien.togetherpayjava.di;

import android.content.Context;

import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.di.builder.ActivityBuilder;
import com.katarzynaiwonakrzemien.togetherpayjava.di.builder.FragmentBuilder;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.IoModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.RepositoryModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.SignedInUserModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.transformer.ModelItemToUiTransformerModule;
import com.katarzynaiwonakrzemien.togetherpayjava.di.module.transformer.ModelUiToItemTransformerModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                ActivityBuilder.class,
                FragmentBuilder.class,
                IoModule.class,
                RepositoryModule.class,
                SignedInUserModule.class,
                ModelItemToUiTransformerModule.class,
                ModelUiToItemTransformerModule.class
        }
)
@Singleton
public interface AppComponent {
    void inject(App app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder appContext(Context app);

        AppComponent build();
    }
}
