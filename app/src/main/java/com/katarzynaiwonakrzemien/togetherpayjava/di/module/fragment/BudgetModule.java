package com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.BudgetSpendingContract;
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.BudgetSpendingPresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;

import dagger.Module;
import dagger.Provides;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.BudgetScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.BudgetTransformer;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.SpendingTransformer;

@Module
public class BudgetModule {
    @Provides
    @BudgetScope
    public BudgetSpendingContract.Presenter provideBudgetPresenter(
            RxSchedulers schedulers, RepositoryManager repository, SignedInUser signedInUser,
            @SpendingTransformer ItemToUiTransformer<Spending, SpendingUi> spendingItemToUiTransformer,
            @BudgetTransformer ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer
    ) {
        return new BudgetSpendingPresenter(schedulers, repository, signedInUser.getUser(), spendingItemToUiTransformer, budgetItemToUiTransformer);
    }
}
