package com.katarzynaiwonakrzemien.togetherpayjava.di.module.transformer;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.budget.BudgetItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.debt.DebtItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.spending.SpendingItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.user.UserItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;
import com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.UserTransformer;

import dagger.Module;
import dagger.Provides;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.BudgetTransformer;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.DebtTransformer;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.SpendingTransformer;

@Module
public class ModelItemToUiTransformerModule {
    @UserTransformer
    @Provides
    public ItemToUiTransformer<User, UserUi> provideUserItemToUiTransformer() {
        return new UserItemToUiTransformer();
    }

    @BudgetTransformer
    @Provides
    public ItemToUiTransformer<Budget, BudgetUi> provideBudgetItemToUiTransformer(
            SignedInUser signedInUser,
            @UserTransformer ItemToUiTransformer<User, UserUi> userItemToUiTransformer,
            @SpendingTransformer ItemToUiTransformer<Spending, SpendingUi> spendingItemToUiTransformer,
            @DebtTransformer ItemToUiTransformer<Debt, DebtUi> debtItemToUiTransformer
    ) {
        return new BudgetItemToUiTransformer(
                signedInUser.getUser(),
                userItemToUiTransformer,
                spendingItemToUiTransformer,
                debtItemToUiTransformer
        );
    }

    @DebtTransformer
    @Provides
    public ItemToUiTransformer<Debt, DebtUi> provideDebtItemToUiTransformer(
            SignedInUser signedInUser,
            @UserTransformer ItemToUiTransformer<User, UserUi> userItemToUiTransformer
    ) {
        return new DebtItemToUiTransformer(signedInUser.getUser(), userItemToUiTransformer);
    }

    @SpendingTransformer
    @Provides
    public ItemToUiTransformer<Spending, SpendingUi> provideSpendingItemToUiTransformer(
            SignedInUser signedInUser,
            @UserTransformer ItemToUiTransformer<User, UserUi> userItemToUiTransformer
    ) {
        return new SpendingItemToUiTransformer(signedInUser.getUser(), userItemToUiTransformer);
    }
}