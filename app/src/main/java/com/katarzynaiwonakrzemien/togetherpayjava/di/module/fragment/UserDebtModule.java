package com.katarzynaiwonakrzemien.togetherpayjava.di.module.fragment;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.contract.user.debt.UserDebtContract;
import com.katarzynaiwonakrzemien.domain.contract.user.debt.UserDebtPresenter;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;

import dagger.Module;
import dagger.Provides;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.UserDebtScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.BudgetTransformer;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.DebtTransformer;

@Module
public class UserDebtModule {
    @Provides
    @UserDebtScope
    public UserDebtContract.Presenter provideUserDebtPresenter(
            RxSchedulers schedulers,
            RepositoryManager repository,
            SignedInUser idSignedUser,
            @DebtTransformer ItemToUiTransformer<Debt, DebtUi> debtItemToUiTransformer,
            @BudgetTransformer ItemToUiTransformer<Budget, BudgetUi> budgetItemToUiTransformer,
            @DebtTransformer UiToItemTransformer<DebtUi, Debt> debtUiToItemTransformer
    ) {
        return new UserDebtPresenter(
                schedulers,
                repository,
                idSignedUser.getUser(),
                debtItemToUiTransformer,
                budgetItemToUiTransformer,
                debtUiToItemTransformer
        );
    }
}