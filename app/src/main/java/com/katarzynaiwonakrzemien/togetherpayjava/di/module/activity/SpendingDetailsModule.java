package com.katarzynaiwonakrzemien.togetherpayjava.di.module.activity;

import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.details.SpendingDetailsContract;
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.details.SpendingDetailsPresenter;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer;

import dagger.Module;
import dagger.Provides;

import static com.katarzynaiwonakrzemien.togetherpayjava.di.Annotations.SpendingDetailsScope;
import static com.katarzynaiwonakrzemien.togetherpayjava.di.Qualifiers.SpendingTransformer;

@Module
public class SpendingDetailsModule {

    @Provides
    @SpendingDetailsScope
    public SpendingDetailsContract.Presenter provideSpendingDetailsPresenter(
            RxSchedulers schedulers,
            RepositoryManager repository,
            @SpendingTransformer ItemToUiTransformer<Spending, SpendingUi> spendingItemToUiTransformer
    ) {
        return new SpendingDetailsPresenter(schedulers, repository, spendingItemToUiTransformer);
    }
}