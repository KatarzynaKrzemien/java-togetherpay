package com.katarzynaiwonakrzemien.togetherpayjava.di.module;

import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.togetherpayjava.rx.AndroidSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class IoModule {
    @Provides
    public RxSchedulers provideRxSchedulers() {
        return new AndroidSchedulers();
    }
}
