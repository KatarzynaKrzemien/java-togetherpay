package com.katarzynaiwonakrzemien.togetherpayjava.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

public class Annotations {

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface BudgetScope {
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface SelectedBudgetScope {
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface BudgetSelectScope {
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface UserDebtScope {
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface BudgetDebtScope {
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface StartScope {
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface UserBudgetScope {
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface SpendingDetailsScope {
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface BudgetDetailsScope {
    }
}
