package com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.debt;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.DiffUtil;

import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericListAdapter;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericViewHolder;

public class BudgetDebtAdapter extends GenericListAdapter<DebtUi> {

    private final BudgetDebtActionListener budgetDebtActionListener;

    public BudgetDebtAdapter(@NonNull DiffUtil.ItemCallback<DebtUi> itemCallback, BudgetDebtActionListener budgetDebtActionListener) {
        super(itemCallback, R.layout.element_budget_debt);
        this.budgetDebtActionListener = budgetDebtActionListener;
    }

    @NonNull
    @Override
    public GenericViewHolder<DebtUi> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BudgetDebtViewHolder(super.inflate(parent), budgetDebtActionListener);
    }

    class BudgetDebtViewHolder extends GenericViewHolder<DebtUi> {
        private final BudgetDebtActionListener budgetDebtActionListener;
        private TextView person = itemView.findViewById(R.id.person);
        private TextView amount = itemView.findViewById(R.id.amount);
        private TextView date = itemView.findViewById(R.id.date);
        private TextView statusInfo = itemView.findViewById(R.id.statusInfo);
        private Button confirm = itemView.findViewById(R.id.confirm);
        private Button reject = itemView.findViewById(R.id.reject);
        private Group confirmRejectButtons = itemView.findViewById(R.id.confirm_reject_buttons_group);
        private TypedArray typedArray = itemView.getContext().getTheme().obtainStyledAttributes(R.styleable.ViewStyle);

        public BudgetDebtViewHolder(@NonNull View itemView, BudgetDebtActionListener budgetDebtActionListener) {
            super(itemView);
            this.budgetDebtActionListener = budgetDebtActionListener;
        }

        @Override
        public void bindTo(DebtUi item) {
            person.setText(getPerson(item));
            amount.setText(App.doubleToRoundedString(item.getAmount().getAmount()));
            date.setText(item.getDate().getTime());
            switch (item.getStatus()) {
                case WAITING:
                    waitingStatusView(item);
                    break;
                case REJECTED:
                    rejectedStatusView();
                    break;
                case CONFIRMED:
                    confirmedStatusView();
                    break;
                default:
                    throw (new IllegalArgumentException());
            }
            itemView.setBackgroundColor(typedArray.getColor(getAdjustViewColors(item), Color.TRANSPARENT));
        }

        private String getPerson(DebtUi item) {
            if (item.getAmount().getBelongToSignInUser()) {
                return itemView.getContext().getString(R.string.you);
            } else {
                return item.getUser().getName();
            }
        }

        private int getAdjustViewColors(DebtUi item) {
            switch (item.getStatus()) {
                case WAITING:
                    return R.styleable.ViewStyle_waitingDebtBackground;
                case REJECTED:
                    return R.styleable.ViewStyle_rejectedDebtBackground;
                case CONFIRMED:
                    return R.styleable.ViewStyle_confirmedDebtBackground;
                default:
                    throw (new IllegalArgumentException());
            }
        }

        private void waitingStatusView(final DebtUi item) {
            if (item.getAmount().getBelongToSignInUser()) {
                adjustVisibilityStatus(false);
                statusInfo.setText(itemView.getContext().getString(R.string.waitingDebtInfo));
            } else {
                adjustVisibilityStatus(true);
                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        budgetDebtActionListener.onDebtConfirmClicked(item);
                    }
                });
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        budgetDebtActionListener.onDebtRejectClicked(item);
                    }
                });
            }
        }

        private void rejectedStatusView() {
            adjustVisibilityStatus(false);
            statusInfo.setText(itemView.getContext().getString(R.string.rejectedDebtInfo));
        }

        private void confirmedStatusView() {
            adjustVisibilityStatus(false);
            statusInfo.setText(itemView.getContext().getString(R.string.confirmDebtInfo));
        }

        private void adjustVisibilityStatus(Boolean isButtonsVisible) {
            if (isButtonsVisible) {
                confirmRejectButtons.setVisibility(View.VISIBLE);
            } else {
                confirmRejectButtons.setVisibility(View.GONE);
            }
            if (isButtonsVisible) {
                statusInfo.setVisibility(View.GONE);
            } else {
                statusInfo.setVisibility(View.VISIBLE);
            }
        }
    }

    interface BudgetDebtActionListener {
        void onDebtConfirmClicked(DebtUi debt);

        void onDebtRejectClicked(DebtUi debt);
    }
}
