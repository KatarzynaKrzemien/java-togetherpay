package com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.debt;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.katarzynaiwonakrzemien.domain.contract.budget.debt.BudgetDebtContract;
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.togetherpayjava.BaseFragment;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericDiffUtilCallback;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.SelectedBudgetActivity;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class BudgetDebtFragment extends BaseFragment<BudgetDebtContract.Presenter, BudgetDebtContract.Action, BudgetDebtContract.Event>
        implements BudgetDebtContract.View, BudgetDebtAdapter.BudgetDebtActionListener {

    private BudgetDebtAdapter adapter = new BudgetDebtAdapter(new GenericDiffUtilCallback<DebtUi>(), this);
    private String idBudget;
    private Double debtAmount = null;
    private RecyclerView recycler;
    private Button repayAllButton;
    private Button repayAmountButton;
    private SwipeRefreshLayout swipeRefresh;
    private TextView noDebtsInfo;
    private TextView info;
    private Group repayGroup;
    private Group debtsGroup;

    @Override
    public void consume(BudgetDebtContract.Event event) {
        if (event instanceof BudgetDebtContract.Event.RenderCurrentDebt) {
            renderCurrentDebt(((BudgetDebtContract.Event.RenderCurrentDebt) event).getDebtInfo());
        } else if (event instanceof BudgetDebtContract.Event.RenderRepaidDebts) {
            renderRepaidDebts(((BudgetDebtContract.Event.RenderRepaidDebts) event).getDebts());
        } else if (event instanceof BudgetDebtContract.Event.RefreshDone) {
            refreshDone();
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_budget_debt;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler = view.findViewById(R.id.recycler);
        repayAllButton = view.findViewById(R.id.repayAllButton);
        repayAmountButton = view.findViewById(R.id.repayAmountButton);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        noDebtsInfo = view.findViewById(R.id.noDebtsInfo);
        info = view.findViewById(R.id.info);
        repayGroup = view.findViewById(R.id.repayGroup);
        debtsGroup = view.findViewById(R.id.debtsGroup);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            idBudget = getArguments().getString(SelectedBudgetActivity.KEY_ID_BUDGET);
        } else {
            throw (new IllegalArgumentException(getString(R.string.errorNoBudgetId)));
        }
        send(new BudgetDebtContract.Action.FetchCurrentDebt(idBudget));
        send(new BudgetDebtContract.Action.FetchRepaidDebts(idBudget));
        repayAmountButton.setOnClickListener(view -> showPayBackDialog());
        repayAllButton.setOnClickListener(view -> send(new BudgetDebtContract.Action.AddDebt(idBudget, debtAmount)));
        swipeRefresh.setOnRefreshListener(() -> send(new BudgetDebtContract.Action.Refresh()));
        recycler.setAdapter(adapter);
    }

    private void showPayBackDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_pay_back, null);
        final TextInputEditText amount = dialogView.findViewById(R.id.amountInput);
        final TextInputLayout amountLayout = dialogView.findViewById(R.id.amountInputLayout);

        new AlertDialog.Builder(requireContext())
                .setView(dialogView)
                .setIcon(R.drawable.ic_repay_debt)
                .setTitle(R.string.payBackSomeMoney)
                .setPositiveButton(R.string.repay, (dialog, which) -> {
                    if ((amount.getText() == null) || (amount.getText().toString().equals(""))) {
                        amountLayout.setError(getString(R.string.errorNoAmount));
                    } else {
                        send(new BudgetDebtContract.Action.AddDebt(idBudget, Double.parseDouble(amount.getText().toString())));
                    }
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                })
                .create()
                .show();
    }

    private void renderCurrentDebt(AmountInfoUi debt) {
        if (debt == null) {
            noDebtsInfo.setVisibility(VISIBLE);
            debtsGroup.setVisibility(GONE);
            repayGroup.setVisibility(GONE);
        } else {
            noDebtsInfo.setVisibility(GONE);
            debtsGroup.setVisibility(VISIBLE);
            if (debt.getBelongToSignInUser()) {
                repayGroup.setVisibility(VISIBLE);
                if (debt.getAmount() != 0.0) {
                    info.setText(getString(R.string.yourDebt, debt.getAmount()));
                } else {
                    info.setText(getString(R.string.noDebts));
                }
            } else {
                repayGroup.setVisibility(GONE);
                if (debt.getAmount() != 0.0) {
                    info.setText(getString(R.string.someoneDebtIs, debt.getAmount()));
                } else {
                    info.setText(getString(R.string.noDebts));
                }
            }
            debtAmount = debt.getAmount();
        }
    }

    private void renderRepaidDebts(List<DebtUi> debts) {
        adapter.submitList(debts);
    }

    private void refreshDone() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onDebtConfirmClicked(DebtUi debt) {
        send(new BudgetDebtContract.Action.ConfirmDebt(debt));
    }

    @Override
    public void onDebtRejectClicked(DebtUi debt) {
        send(new BudgetDebtContract.Action.RejectDebt(debt));
    }
}
