package com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.details;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.katarzynaiwonakrzemien.domain.contract.budget.details.BudgetDetailsContract;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.togetherpayjava.BaseFragment;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.SelectedBudgetActivity;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.user.UserBudgetActivity;

public class BudgetDetailsFragment
        extends BaseFragment<BudgetDetailsContract.Presenter, BudgetDetailsContract.Action, BudgetDetailsContract.Event>
        implements BudgetDetailsContract.View {

    private String idBudget;
    private BudgetUi budget;
    private TextView otherUser;
    private TextInputEditText nameInput;
    private TextInputLayout nameInputLayout;
    private ImageButton save;
    private AppCompatButton delete;
    private ImageButton revert;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_budget_details;
    }

    @Override
    protected void consume(BudgetDetailsContract.Event event) {
        if (event instanceof BudgetDetailsContract.Event.RenderBudget) {
            renderBudget(((BudgetDetailsContract.Event.RenderBudget) event).getBudget());
        }
    }

    private void renderBudget(BudgetUi budget) {
        otherUser.setText(budget.getOtherUser().getName());
        nameInput.setText(budget.getName());
        this.budget = budget;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        otherUser = view.findViewById(R.id.otherUser);
        nameInput = view.findViewById(R.id.nameInput);
        nameInputLayout = view.findViewById(R.id.nameInputLayout);
        save = view.findViewById(R.id.save);
        delete = view.findViewById(R.id.delete);
        revert = view.findViewById(R.id.revert);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            idBudget = getArguments().getString(SelectedBudgetActivity.KEY_ID_BUDGET);
        } else {
            throw (new IllegalArgumentException(getString(R.string.errorNoBudgetId)));
        }

        send(new BudgetDetailsContract.Action.FetchBudget(idBudget));

        delete.setOnClickListener(view -> new AlertDialog.Builder(requireContext())
                .setIcon(R.drawable.ic_delete_dark)
                .setTitle(R.string.deleteBudget)
                .setMessage(getString(R.string.doYouWantDeleteBudget, budget.getName()))
                .setPositiveButton(R.string.delete, (dialog, which) -> {
                    send(new BudgetDetailsContract.Action.DeleteBudget(budget));
                    dialog.dismiss();
                    startActivity(UserBudgetActivity.getIntent(requireContext()));
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss())
                .create()
                .show());

        revert.setOnClickListener(view -> renderBudget(budget));

        save.setOnClickListener(view -> {
            if (nameInput.getText() == null || nameInput.getText().toString().equals("")) {
                nameInputLayout.setError(getString(R.string.errorNoBudgetName));
            } else {
                send(new BudgetDetailsContract.Action.UpdateBudget(nameInput.getText().toString(), budget));
                nameInputLayout.setError(null);
            }
        });
    }
}
