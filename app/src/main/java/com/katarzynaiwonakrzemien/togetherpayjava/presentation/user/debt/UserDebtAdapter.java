package com.katarzynaiwonakrzemien.togetherpayjava.presentation.user.debt;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.DiffUtil;

import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericListAdapter;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericViewHolder;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class UserDebtAdapter extends GenericListAdapter<DebtUi> {

    private final UserDebtActionListener userDebtActionListener;

    public UserDebtAdapter(@NonNull DiffUtil.ItemCallback<DebtUi> itemCallback, UserDebtActionListener userDebtActionListener) {
        super(itemCallback, R.layout.element_user_debt);
        this.userDebtActionListener = userDebtActionListener;
    }

    @NonNull
    @Override
    public GenericViewHolder<DebtUi> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DebtViewHolder(super.inflate(parent), userDebtActionListener);
    }

    class DebtViewHolder extends GenericViewHolder<DebtUi> {
        private final UserDebtActionListener userDebtActionListener;
        private TextView person = itemView.findViewById(R.id.person);
        private TextView amount = itemView.findViewById(R.id.amount);
        private TextView date = itemView.findViewById(R.id.date);
        private TextView statusInfo = itemView.findViewById(R.id.statusInfo);
        private Button confirm = itemView.findViewById(R.id.confirm);
        private Button reject = itemView.findViewById(R.id.reject);
        private Button repayAll = itemView.findViewById(R.id.repayAllButton);
        private Button repayAmount = itemView.findViewById(R.id.repayAmountButton);
        private Group dateAmount = itemView.findViewById(R.id.date_amount_group);
        private Group repayButtons = itemView.findViewById(R.id.repay_buttons_group);
        private Group confirmRejectButtons = itemView.findViewById(R.id.confirm_reject_buttons_group);
        private TypedArray typedArray = itemView.getContext().getTheme().obtainStyledAttributes(R.styleable.ViewStyle);

        public DebtViewHolder(@NonNull View itemView, UserDebtActionListener userDebtActionListener) {
            super(itemView);
            this.userDebtActionListener = userDebtActionListener;
        }

        @Override
        public void bindTo(DebtUi item) {
            adjustVisibility(item);
            fillInView(item);
            itemView.setBackgroundColor(typedArray.getColor(getAdjustViewColors(item), Color.TRANSPARENT));
        }

        private String getPerson(DebtUi item) {
            if (item.getAmount().getBelongToSignInUser()) {
                return itemView.getContext().getString(R.string.you);
            } else {
                return item.getUser().getName();
            }
        }

        private int getAdjustViewColors(DebtUi item) {
            switch (item.getStatus()) {
                case WAITING:
                    return R.styleable.ViewStyle_waitingDebtBackground;
                case REJECTED:
                    return R.styleable.ViewStyle_rejectedDebtBackground;
                case CONFIRMED:
                    return R.styleable.ViewStyle_confirmedDebtBackground;
                case NOREPAY:
                    return R.styleable.ViewStyle_noRepayDebtBackground;
                default:
                    throw (new IllegalArgumentException());
            }
        }

        private void fillInView(final DebtUi item) {
            person.setText(getPerson(item));
            amount.setText(App.doubleToRoundedString(item.getAmount().getAmount()));
            if (item.getDate().getTime().equals("")) {
                date.setText(itemView.getContext().getString(R.string.debtToRapay));
            } else {
                date.setText(item.getDate().getTime());
            }
            switch (item.getStatus()) {
                case WAITING:
                    if (item.getAmount().getBelongToSignInUser()) {
                        statusInfo.setText(itemView.getContext().getString(R.string.waitingDebtInfo));
                    } else {
                        confirm.setOnClickListener(view -> userDebtActionListener.onDebtConfirmClicked(item));
                        reject.setOnClickListener(view -> userDebtActionListener.onDebtRejectClicked(item));
                    }
                    break;
                case REJECTED:
                    statusInfo.setText(itemView.getContext().getString(R.string.rejectedDebtInfo));
                    break;
                case CONFIRMED:
                    statusInfo.setText(itemView.getContext().getString(R.string.confirmDebtInfo));
                    break;
                case NOREPAY:
                    statusInfo.setText(itemView.getContext().getString(R.string.repayYourDebt, item.getAmount().getAmount()));
                    repayAmount.setOnClickListener(view -> userDebtActionListener.onDebtRepayAmountClicked(item.getIdBudget()));
                    repayAll.setOnClickListener(view -> userDebtActionListener.onDebtRepayAllClicked(item.getIdBudget(), item.getAmount().getAmount()));
                    break;
            }
        }

        private void adjustVisibility(DebtUi item) {
            switch (item.getStatus()) {
                case NOREPAY:
                    if (item.getAmount().getBelongToSignInUser()) {
                        confirmRejectButtons.setVisibility(GONE);
                        dateAmount.setVisibility(GONE);
                        statusInfo.setVisibility(VISIBLE);
                        repayButtons.setVisibility(VISIBLE);
                    } else {
                        confirmRejectButtons.setVisibility(GONE);
                        dateAmount.setVisibility(VISIBLE);
                        statusInfo.setVisibility(GONE);
                        repayButtons.setVisibility(GONE);
                    }
                    break;
                case CONFIRMED:
                case REJECTED:
                    confirmRejectButtons.setVisibility(GONE);
                    dateAmount.setVisibility(VISIBLE);
                    statusInfo.setVisibility(VISIBLE);
                    repayButtons.setVisibility(GONE);
                    break;
                case WAITING:
                    if (item.getAmount().getBelongToSignInUser()) {
                        confirmRejectButtons.setVisibility(GONE);
                        dateAmount.setVisibility(VISIBLE);
                        statusInfo.setVisibility(VISIBLE);
                        repayButtons.setVisibility(GONE);
                    } else {
                        confirmRejectButtons.setVisibility(VISIBLE);
                        dateAmount.setVisibility(VISIBLE);
                        statusInfo.setVisibility(GONE);
                        repayButtons.setVisibility(GONE);
                    }
                    break;
            }
        }
    }

    interface UserDebtActionListener {
        void onDebtConfirmClicked(DebtUi debt);

        void onDebtRejectClicked(DebtUi debt);

        void onDebtRepayAmountClicked(String idBudget);

        void onDebtRepayAllClicked(String idBudget, double amount);
    }
}
