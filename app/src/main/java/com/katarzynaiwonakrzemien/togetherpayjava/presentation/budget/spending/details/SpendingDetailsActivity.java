package com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.spending.details;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.details.SpendingDetailsContract;
import com.katarzynaiwonakrzemien.domain.model.DateUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.BaseActivity;
import com.katarzynaiwonakrzemien.togetherpayjava.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class SpendingDetailsActivity
        extends BaseActivity<SpendingDetailsContract.Presenter, SpendingDetailsContract.Action, SpendingDetailsContract.Event>
        implements SpendingDetailsContract.View {

    public static final String KEY_ID_SPENDING = "idSpending";
    public static final String KEY_ID_BUDGET = "idBudget";

    public static Intent getIntent(Context context, String idSpending, String idBudget) {
        Intent intent = new Intent(context, SpendingDetailsActivity.class);
        intent.putExtra(KEY_ID_BUDGET, idBudget);
        intent.putExtra(KEY_ID_SPENDING, idSpending);
        return intent;
    }

    private String idBudget;
    private String idSpending;
    private SpendingUi spending = null;
    private Map<String, String> users;
    private FloatingActionButton delete;
    private Spinner usersSpinner;
    private TextInputLayout productInputLayout;
    private TextInputEditText productInput;
    private TextView date;
    private TextInputLayout amountInputLayout;
    private TextInputEditText amountInput;
    private ImageButton cancel;
    private ImageButton ok;
    private ConstraintLayout root;

    @Override
    protected void consume(SpendingDetailsContract.Event event) {
        if (event instanceof SpendingDetailsContract.Event.RenderSpending) {
            renderSpending(((SpendingDetailsContract.Event.RenderSpending) event).getSpending());
        } else if (event instanceof SpendingDetailsContract.Event.RenderSelectedBudgetUsers) {
            renderSelectedBudgetUsers(((SpendingDetailsContract.Event.RenderSelectedBudgetUsers) event).getUsers());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spending_details);
        findViews();
        setupSavedBudgetArguments(savedInstanceState);
        root.setTransitionName(idSpending);
    }

    private void findViews() {
        delete = findViewById(R.id.delete);
        usersSpinner = findViewById(R.id.usersSpinner);
        productInputLayout = findViewById(R.id.productInputLayout);
        productInput = findViewById(R.id.productInput);
        date = findViewById(R.id.date);
        amountInputLayout = findViewById(R.id.amountInputLayout);
        amountInput = findViewById(R.id.amountInput);
        cancel = findViewById(R.id.cancel);
        ok = findViewById(R.id.ok);
        root = findViewById(R.id.root);
    }

    private void setupSavedBudgetArguments(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_ID_SPENDING)) {
            idSpending = savedInstanceState.getString(KEY_ID_SPENDING);
        } else {
            idSpending = getIntent().getStringExtra(KEY_ID_SPENDING);
        }
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_ID_BUDGET)) {
            idBudget = savedInstanceState.getString(KEY_ID_BUDGET);
        } else {
            idBudget = getIntent().getStringExtra(KEY_ID_BUDGET);
        }
        send(new SpendingDetailsContract.Action.FetchSpending(idBudget, idSpending));
    }

    private void renderSpending(SpendingUi spendingItem) {
        if (spending != spendingItem) {
            spending = spendingItem;
            setupViews();
        }
    }

    private void renderSelectedBudgetUsers(Map<String, String> users) {
        this.users = users;
        usersSpinner.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, new ArrayList<>(users.keySet())));
        usersSpinner.setSelection(new ArrayList<>(users.keySet()).indexOf(spending.getUser().getName()));
    }

    private void setupViews() {
        setupUserSpinner();
        setupBackground();
        setupProduct();
        setupDate();
        setupAmount();
        setupButtons();
    }

    private void setupUserSpinner() {
        send(new SpendingDetailsContract.Action.FetchSelectedBudgetUsers(idBudget));
    }

    private void setupBackground() {
        TypedArray typedArray = getTheme().obtainStyledAttributes(R.styleable.ViewStyle);
        root.setBackgroundColor(typedArray.getColor(getBackgroundColor(), Color.TRANSPARENT));
    }

    private int getBackgroundColor() {
        if (spending.getAmount().getBelongToSignInUser()) {
            return R.styleable.ViewStyle_userSpendingBackground;
        } else {
            return R.styleable.ViewStyle_otherUserSpendingBackground;
        }
    }

    private void setupProduct() {
        productInput.setText(spending.getProduct());
    }

    private void setupDate() {
        final DateUi[] dateText = {null};
        date.setText(spending.getDate().getTime());
        date.setOnClickListener(view -> {
            View dialogView = getLayoutInflater().inflate(R.layout.dialog_date_picker, null);
            final DatePicker datePicker = dialogView.findViewById(R.id.datePicker);
            datePicker.setMaxDate(Calendar.getInstance().getTimeInMillis());
            if (dateText[0] == null) {
                datePicker.updateDate(spending.getDate().getYear(), spending.getDate().getMonth(), spending.getDate().getDay());
            } else {
                datePicker.updateDate(dateText[0].getYear(), dateText[0].getMonth(), dateText[0].getDay());
            }
            new AlertDialog.Builder(SpendingDetailsActivity.this)
                    .setView(dialogView)
                    .setIcon(R.drawable.ic_spending_date)
                    .setTitle(R.string.pickDate)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                        dateText[0] = new DateUi(DateUi.createDate(datePicker.getDayOfMonth(), datePicker.getMonth(), datePicker.getYear()));
                        date.setText(dateText[0].getTime());
                        dialog.dismiss();
                    })
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss())
                    .create()
                    .show();
        });
    }


    private void setupAmount() {
        amountInput.setText(App.doubleToRoundedString(spending.getAmount().getAmount()));
    }

    private void setupButtons() {
        ok.setOnClickListener(view -> {
            if (productInput.getText() == null || productInput.getText().toString().equals("")) {
                productInputLayout.setError(getString(R.string.errorNoProductName));
            } else if (amountInput.getText() == null || amountInput.getText().toString().equals("")) {
                amountInputLayout.setError(getString(R.string.errorNoAmount));
                productInputLayout.setError(null);

            } else {
                send(new SpendingDetailsContract.Action.UpdateSpending(
                        idBudget,
                        idSpending,
                        users.get(usersSpinner.getSelectedItem().toString()),
                        usersSpinner.getSelectedItem().toString(),
                        productInput.getText().toString(),
                        date.getText().toString(),
                        Double.parseDouble(amountInput.getText().toString())
                ));
                onBackPressed();
                finish();
            }
        });

        cancel.setOnClickListener(view -> {
            onBackPressed();
            finish();
        });

        delete.setOnClickListener(view -> new AlertDialog.Builder(SpendingDetailsActivity.this)
                .setIcon(R.drawable.ic_delete_dark)
                .setTitle(R.string.deleteSpending)
                .setMessage(R.string.doYouWantDeleteSpending)
                .setPositiveButton(R.string.delete, (dialog, which) -> {
                    send(new SpendingDetailsContract.Action.DeleteSpending(idSpending));
                    dialog.dismiss();
                    onBackPressed();
                    finish();
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss())
                .create()
                .show());
    }
}
