package com.katarzynaiwonakrzemien.togetherpayjava.presentation.user.debt;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.katarzynaiwonakrzemien.domain.contract.user.debt.UserDebtContract;
import com.katarzynaiwonakrzemien.domain.model.DebtUi;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.BaseFragment;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericDiffUtilCallback;

import java.util.List;

public class UserDebtFragment
        extends BaseFragment<UserDebtContract.Presenter, UserDebtContract.Action, UserDebtContract.Event>
        implements UserDebtContract.View, UserDebtAdapter.UserDebtActionListener {

    private UserDebtAdapter adapter = new UserDebtAdapter(new GenericDiffUtilCallback<DebtUi>(), this);

    private TextView myDebt;
    private TextView oweMe;
    private RecyclerView recycler;
    private SwipeRefreshLayout swipeRefresh;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_user_debt;
    }

    @Override
    protected void consume(UserDebtContract.Event event) {
        if (event instanceof UserDebtContract.Event.RenderDebts) {
            renderDebts(((UserDebtContract.Event.RenderDebts) event).getDebts());
        } else if (event instanceof UserDebtContract.Event.RenderDebtSum) {
            renderDebtSum(((UserDebtContract.Event.RenderDebtSum) event).getDebt());
        } else if (event instanceof UserDebtContract.Event.RenderOwingSum) {
            renderOwingSum(((UserDebtContract.Event.RenderOwingSum) event).getOwing());
        } else if (event instanceof UserDebtContract.Event.RefreshDone) {
            refreshDone();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myDebt = view.findViewById(R.id.myDebt);
        oweMe = view.findViewById(R.id.oweMe);
        recycler = view.findViewById(R.id.recycler);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recycler.setAdapter(adapter);
        send(new UserDebtContract.Action.FetchDebts());
        swipeRefresh.setOnRefreshListener(() -> send(new UserDebtContract.Action.Refresh()));
        send(new UserDebtContract.Action.FetchDebtSum());
        send(new UserDebtContract.Action.FetchOwingSum());
    }

    private void renderDebts(List<DebtUi> debts) {
        adapter.submitList(debts);
    }

    private void renderDebtSum(double debt) {
        myDebt.setText(App.doubleToRoundedString(debt));
    }

    private void renderOwingSum(double owing) {
        oweMe.setText(App.doubleToRoundedString(owing));
    }

    private void refreshDone() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onDebtConfirmClicked(DebtUi debt) {
        send(new UserDebtContract.Action.ConfirmDebt(debt));
    }

    @Override
    public void onDebtRejectClicked(DebtUi debt) {
        send(new UserDebtContract.Action.RejectDebt(debt));
    }

    @Override
    public void onDebtRepayAmountClicked(final String idBudget) {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_pay_back, null);
        final TextInputEditText amount = dialogView.findViewById(R.id.amountInput);
        final TextInputLayout amountLayout = dialogView.findViewById(R.id.amountInputLayout);

        new AlertDialog.Builder(requireContext())
                .setView(dialogView)
                .setIcon(R.drawable.ic_repay_debt)
                .setTitle(R.string.payBackSomeMoney)
                .setPositiveButton(R.string.repay, (dialog, which) -> {
                    if (amount.getText() == null || amount.getText().toString().equals("")) {
                        amountLayout.setError(getString(R.string.errorNoAmount));
                    } else {
                        send(new UserDebtContract.Action.AddDebt(idBudget, Double.parseDouble(amount.getText().toString())));
                    }
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                })
                .create()
                .show();
    }

    @Override
    public void onDebtRepayAllClicked(String idBudget, double amount) {
        send(new UserDebtContract.Action.AddDebt(idBudget, amount));
    }
}