package com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.spending;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.BudgetSpendingContract;
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi;
import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.BaseFragment;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericDiffUtilCallback;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.SelectedBudgetActivity;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.spending.details.SpendingDetailsActivity;

import java.util.List;

public class BudgetSpendingFragment
        extends BaseFragment<BudgetSpendingContract.Presenter, BudgetSpendingContract.Action, BudgetSpendingContract.Event>
        implements BudgetSpendingContract.View, BudgetSpendingAdapter.BudgetSpendingActionListener {

    private BudgetSpendingAdapter adapter = new BudgetSpendingAdapter(new GenericDiffUtilCallback<SpendingUi>(), this);
    private String idBudget;
    private SwipeRefreshLayout swipeRefresh;
    private TextView spending;
    private TextView debt;
    private TextView debtTitle;
    private RecyclerView recycler;
    private FloatingActionButton fabAdd;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_budget;
    }

    @Override
    protected void consume(BudgetSpendingContract.Event event) {
        if (event instanceof BudgetSpendingContract.Event.RenderSpending) {
            renderSpending(((BudgetSpendingContract.Event.RenderSpending) event).getSpending());
        } else if (event instanceof BudgetSpendingContract.Event.RenderCurrentDebt) {
            renderCurrentDebt(((BudgetSpendingContract.Event.RenderCurrentDebt) event).getDebtInfo());
        } else if (event instanceof BudgetSpendingContract.Event.RenderSignInUserSpendingSum) {
            renderSignInUserSpendingSum(((BudgetSpendingContract.Event.RenderSignInUserSpendingSum) event).getAmount());
        } else if (event instanceof BudgetSpendingContract.Event.RefreshDone) {
            refreshDone();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        spending = view.findViewById(R.id.spending);
        debt = view.findViewById(R.id.debt);
        debtTitle = view.findViewById(R.id.debtTitle);
        recycler = view.findViewById(R.id.recycler);
        fabAdd = view.findViewById(R.id.fabAdd);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            idBudget = getArguments().getString(SelectedBudgetActivity.KEY_ID_BUDGET);
        } else {
            throw (new IllegalArgumentException(getString(R.string.errorNoBudgetId)));
        }
        send(new BudgetSpendingContract.Action.FetchSpending(idBudget));
        send(new BudgetSpendingContract.Action.FetchSignInUserSpendingSum(idBudget));
        send(new BudgetSpendingContract.Action.FetchCurrentDebt(idBudget));
        recycler.setAdapter(adapter);
        fabAdd.setOnClickListener(view -> showAddSpendingDialog());
        swipeRefresh.setOnRefreshListener(() -> send(new BudgetSpendingContract.Action.Refresh()));
    }

    private void showAddSpendingDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_add_spending, null);
        final TextInputEditText product = dialogView.findViewById(R.id.productInput);
        final TextInputEditText amount = dialogView.findViewById(R.id.amountInput);
        final TextInputLayout amountLayout = dialogView.findViewById(R.id.amountInputLayout);
        final TextInputLayout productLayout = dialogView.findViewById(R.id.productInputLayout);

        new AlertDialog.Builder(requireContext())
                .setView(dialogView)
                .setIcon(R.drawable.ic_add)
                .setTitle(R.string.add_spending)
                .setPositiveButton(R.string.add, (dialog, which) -> {
                    if (product.getText() == null || product.getText().toString().equals("")) {
                        productLayout.setError(getString(R.string.errorNoProductName));
                    } else if (amount.getText() == null || amount.getText().toString().equals("")) {
                        amountLayout.setError(getString(R.string.errorNoAmount));
                        productLayout.setError(null);
                    } else {
                        send(new BudgetSpendingContract.Action.AddSpending(
                                idBudget,
                                product.getText().toString(),
                                Double.parseDouble(amount.getText().toString())
                        ));
                    }
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                })
                .create()
                .show();
    }

    private void renderCurrentDebt(AmountInfoUi debtInfo) {
        if (debtInfo.getAmount() == 0.0) {
            debtTitle.setText(getString(R.string.noDebts));
            debt.setText(getString(R.string.empty));
        } else {
            debt.setText(App.doubleToRoundedString(debtInfo.getAmount()));
            if (debtInfo.getBelongToSignInUser()) {
                debtTitle.setText(getString(R.string.myDebt));
            } else {
                debtTitle.setText(getString(R.string.oweMe));
            }
        }
    }

    private void renderSignInUserSpendingSum(double amount) {
        spending.setText(App.doubleToRoundedString(amount));
    }

    private void renderSpending(List<SpendingUi> spending) {
        adapter.submitList(spending);
    }


    private void refreshDone() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onSpendingClicked(SpendingUi spending, View sharedView) {
        Intent intent = SpendingDetailsActivity.getIntent(requireContext(), spending.getId(), idBudget);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity(), sharedView, spending.getId());
        startActivity(intent, options.toBundle());
    }
}