package com.katarzynaiwonakrzemien.togetherpayjava.presentation.user.budgets;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericListAdapter;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericViewHolder;

public class BudgetSelectAdapter extends GenericListAdapter<BudgetUi> {

    private final BudgetActionListener budgetActionListener;

    public BudgetSelectAdapter(@NonNull DiffUtil.ItemCallback<BudgetUi> itemCallback, BudgetActionListener budgetActionListener) {
        super(itemCallback, R.layout.element_budget);
        this.budgetActionListener = budgetActionListener;
    }

    @NonNull
    @Override
    public GenericViewHolder<BudgetUi> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BudgetViewHolder(super.inflate(parent), budgetActionListener);
    }

    class BudgetViewHolder extends GenericViewHolder<BudgetUi> {
        private final BudgetActionListener budgetActionListener;
        private TextView otherUser = itemView.findViewById(R.id.otherUser);
        private TextView name = itemView.findViewById(R.id.name);
        private TextView debt = itemView.findViewById(R.id.debt);
        private TypedArray typedArray = itemView.getContext().getTheme().obtainStyledAttributes(R.styleable.ViewStyle);

        public BudgetViewHolder(@NonNull View itemView, BudgetActionListener budgetActionListener) {
            super(itemView);
            this.budgetActionListener = budgetActionListener;
        }

        @Override
        public void bindTo(final BudgetUi item) {
            name.setText(item.getName());
            otherUser.setText(item.getOtherUser().getName());
            if (item.getDebt().getAmount() == 0.0) {
                debt.setText(itemView.getContext().getString(R.string.noDebts));
                debt.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                itemView.setBackgroundColor(typedArray.getColor(R.styleable.ViewStyle_noDebtBudgetBackground, Color.TRANSPARENT));
            } else {
                debt.setText(App.doubleToRoundedString(item.getDebt().getAmount()));
                debt.setCompoundDrawablesWithIntrinsicBounds(itemView.getContext().getDrawable(getDebtDrawable(item)), null, null, null);
                itemView.setBackgroundColor(typedArray.getColor(getAdjustViewColor(item), Color.TRANSPARENT));
            }
            itemView.setOnClickListener(view -> budgetActionListener.onBudgetClicked(item));
        }

        private int getDebtDrawable(BudgetUi item) {
            if (item.getDebt().getBelongToSignInUser()) {
                return R.drawable.ic_debt_right;
            } else {
                return R.drawable.ic_debt_left;
            }
        }


        private int getAdjustViewColor(BudgetUi item) {
            if (item.getDebt().getBelongToSignInUser()) {
                return R.styleable.ViewStyle_debtBudgetBackground;
            } else {
                return R.styleable.ViewStyle_owingBudgetBackground;
            }
        }
    }

    interface BudgetActionListener {
        void onBudgetClicked(BudgetUi budget);
    }
}
