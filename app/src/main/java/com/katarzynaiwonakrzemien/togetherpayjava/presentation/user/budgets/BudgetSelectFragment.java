package com.katarzynaiwonakrzemien.togetherpayjava.presentation.user.budgets;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.katarzynaiwonakrzemien.domain.contract.user.budget.BudgetSelectContract;
import com.katarzynaiwonakrzemien.domain.model.BudgetUi;
import com.katarzynaiwonakrzemien.domain.model.UserUi;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.BaseFragment;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericDiffUtilCallback;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.SelectedBudgetActivity;

import java.util.ArrayList;
import java.util.List;

public class BudgetSelectFragment
        extends BaseFragment<BudgetSelectContract.Presenter, BudgetSelectContract.Action, BudgetSelectContract.Event>
        implements BudgetSelectContract.View, BudgetSelectAdapter.BudgetActionListener {

    private BudgetSelectAdapter adapter = new BudgetSelectAdapter(new GenericDiffUtilCallback<BudgetUi>(), this);
    private List<UserUi> users = new ArrayList<>();
    private SwipeRefreshLayout swipeRefresh;
    private TextView spending;
    private TextView myDebt;
    private TextView oweMe;
    private RecyclerView recycler;
    private FloatingActionButton fabAdd;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_budget_select;
    }

    @Override
    protected void consume(BudgetSelectContract.Event event) {
        if (event instanceof BudgetSelectContract.Event.RenderBudgets) {
            renderBudgets(((BudgetSelectContract.Event.RenderBudgets) event).getBudgets());
        } else if (event instanceof BudgetSelectContract.Event.RenderUsers) {
            renderUsers(((BudgetSelectContract.Event.RenderUsers) event).getUsers());
        } else if (event instanceof BudgetSelectContract.Event.RenderDebtSum) {
            renderDebtSum(((BudgetSelectContract.Event.RenderDebtSum) event).getAmount());
        } else if (event instanceof BudgetSelectContract.Event.RenderOwingSum) {
            renderOwingSum(((BudgetSelectContract.Event.RenderOwingSum) event).getAmount());
        } else if (event instanceof BudgetSelectContract.Event.RenderSignInUserSpendingSum) {
            renderSignInUserSpendingSum(((BudgetSelectContract.Event.RenderSignInUserSpendingSum) event).getAmount());
        } else if (event instanceof BudgetSelectContract.Event.RefreshDone) {
            refreshDone();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        spending = view.findViewById(R.id.spending);
        myDebt = view.findViewById(R.id.myDebt);
        oweMe = view.findViewById(R.id.oweMe);
        recycler = view.findViewById(R.id.recycler);
        fabAdd = view.findViewById(R.id.fabAdd);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        send(new BudgetSelectContract.Action.FetchBudgets());
        send(new BudgetSelectContract.Action.FetchDebtSum());
        send(new BudgetSelectContract.Action.FetchOwingSum());
        send(new BudgetSelectContract.Action.FetchSignInUserSpendingSum());
        send(new BudgetSelectContract.Action.FetchUsers());
        recycler.setAdapter(adapter);
        fabAdd.setOnClickListener(view -> showAddBudgetDialog());
        swipeRefresh.setOnRefreshListener(() -> send(new BudgetSelectContract.Action.Refresh()));
    }

    private void showAddBudgetDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_add_budget, null);
        final TextInputEditText name = dialogView.findViewById(R.id.name);
        final TextInputLayout nameLayout = dialogView.findViewById(R.id.nameInputLayout);
        final List<String> usersNames = new ArrayList<>();
        for (UserUi user : users) {
            usersNames.add(user.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.select_dialog_item, usersNames);
        final UserUi[] otherUserUi = {null};
        final AppCompatAutoCompleteTextView otherUser = dialogView.findViewById(R.id.otherUser);
        otherUser.setOnItemClickListener((adapterView, view, i, l) -> {
            for (UserUi user : users) {
                if (user.getName().equals(adapterView.getItemAtPosition(i))) {
                    otherUserUi[0] = user;
                }

            }
        });
        otherUser.setAdapter(adapter);

        new AlertDialog.Builder(requireContext())
                .setView(dialogView)
                .setIcon(R.drawable.ic_add)
                .setTitle(R.string.add_budget)
                .setPositiveButton(R.string.add, (dialog, which) -> {
                    if (name.getText() == null || name.getText().toString().equals("")) {
                        nameLayout.setError(getString(R.string.errorNoBudgetName));
                    } else if (otherUserUi[0] == null) {
                        otherUser.setError(getString(R.string.errorNoOtherUser));
                        nameLayout.setError(null);
                    } else {
                        send(new BudgetSelectContract.Action.AddBudget(name.getText().toString(), otherUserUi[0]));
                    }
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                })
                .create()
                .show();
    }

    private void renderOwingSum(Double amount) {
        oweMe.setText(App.doubleToRoundedString(amount));
    }

    private void renderDebtSum(Double amount) {
        myDebt.setText(App.doubleToRoundedString(amount));
    }

    private void renderSignInUserSpendingSum(Double amount) {
        spending.setText(App.doubleToRoundedString(amount));
    }

    private void renderBudgets(List<BudgetUi> budgets) {
        adapter.submitList(budgets);
    }

    private void renderUsers(List<UserUi> users) {
        this.users = users;
    }

    private void refreshDone() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onBudgetClicked(BudgetUi budget) {
        startActivity(SelectedBudgetActivity.getIntent(requireContext(), budget.getId()));
    }
}