package com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.ArraySet;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.navigation.NavArgument;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavGraph;
import androidx.navigation.NavType;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.katarzynaiwonakrzemien.domain.contract.budget.SelectedBudgetContract;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.BaseActivity;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.start.StartActivity;

import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class SelectedBudgetActivity extends BaseActivity<SelectedBudgetContract.Presenter, SelectedBudgetContract.Action, SelectedBudgetContract.Event>
        implements SelectedBudgetContract.View, HasAndroidInjector {

    public static final String KEY_ID_BUDGET = "key_id_budget";

    public static Intent getIntent(Context context, String idBudget) {
        Intent intent = new Intent(context, SelectedBudgetActivity.class);
        intent.putExtra(KEY_ID_BUDGET, idBudget);
        return intent;
    }

    @Inject
    DispatchingAndroidInjector<Object> fragmentInjector;
    private String idBudget;
    private String nameBudget = "";

    @Override
    protected void consume(SelectedBudgetContract.Event event) {
        if (event instanceof SelectedBudgetContract.Event.RenderSignedInUserName) {
            renderSignedInUserName(((SelectedBudgetContract.Event.RenderSignedInUserName) event).getUserName());
        } else if (event instanceof SelectedBudgetContract.Event.RenderBudgetName) {
            renderBudgetName(((SelectedBudgetContract.Event.RenderBudgetName) event).getBudgetName());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_budget);
        setupSavedBudgetArguments(savedInstanceState);
        send(new SelectedBudgetContract.Action.FetchBudgetName(idBudget));
        setupInfoBar();
        setSupportActionBar(findViewById(R.id.toolbar));
    }


    private void setupGraphNav(String budgetName) {
        NavController navController = Navigation.findNavController(this, R.id.navigationHost);
        NavigationUI.setupWithNavController((BottomNavigationView) findViewById(R.id.navigationView), navController);
        navController.setGraph(R.navigation.selected_budget_navigation, getNavArgs());
        Objects.requireNonNull(navController.getGraph().findNode(R.id.budget_fragment)).addArgument(KEY_ID_BUDGET, new NavArgument.Builder().setType(NavType.StringType).setDefaultValue(idBudget).build());
        Objects.requireNonNull(navController.getGraph().findNode(R.id.budget_fragment)).setLabel(budgetName);
        Objects.requireNonNull(navController.getGraph().findNode(R.id.debt_fragment)).addArgument(KEY_ID_BUDGET, new NavArgument.Builder().setType(NavType.StringType).setDefaultValue(idBudget).build());
        Objects.requireNonNull(navController.getGraph().findNode(R.id.debt_fragment)).setLabel(getString(R.string.budget_name_debts, budgetName));
        Objects.requireNonNull(navController.getGraph().findNode(R.id.details_fragment)).addArgument(KEY_ID_BUDGET, new NavArgument.Builder().setType(NavType.StringType).setDefaultValue(idBudget).build());
        Objects.requireNonNull(navController.getGraph().findNode(R.id.details_fragment)).setLabel(getString(R.string.budgetDetails, budgetName));
        AppBarConfiguration config = new AppBarConfiguration.Builder(getIdNavDestinationSet(navController.getGraph())).build();
        setSupportActionBar(findViewById(R.id.toolbar));
        NavigationUI.setupActionBarWithNavController(this, navController, config);
    }

    private Bundle getNavArgs() {
        Bundle args = new Bundle();
        args.putString(KEY_ID_BUDGET, idBudget);
        return args;
    }

    private void setupSavedBudgetArguments(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_ID_BUDGET)) {
            idBudget = savedInstanceState.getString(KEY_ID_BUDGET);
        } else {
            idBudget = getIntent().getStringExtra(KEY_ID_BUDGET);
        }
    }

    private void setupInfoBar() {
        ((TextView) findViewById(R.id.today)).setText(App.date);
        setupSignedOut();
        send(new SelectedBudgetContract.Action.FetchSignedInUserName());
    }

    private void setupSignedOut() {
        findViewById(R.id.signOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAffinity();
                startActivity(StartActivity.getIntent(SelectedBudgetActivity.this, true));
            }
        });
    }

    private Set<Integer> getIdNavDestinationSet(NavGraph navGraph) {
        Set<Integer> set = new ArraySet<>();
        for (NavDestination navDestination : navGraph) {
            set.add(navDestination.getId());
        }
        return set;
    }

    private void renderSignedInUserName(String userName) {
        ((TextView) findViewById(R.id.signedInUserName)).setText(userName);
    }

    private void renderBudgetName(String budgetName) {
        if (!budgetName.equals(nameBudget)) {
            nameBudget = budgetName;
            setupGraphNav(budgetName);
        }
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return fragmentInjector;
    }
}
