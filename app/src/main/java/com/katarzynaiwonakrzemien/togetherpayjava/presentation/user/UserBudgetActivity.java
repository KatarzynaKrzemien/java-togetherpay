package com.katarzynaiwonakrzemien.togetherpayjava.presentation.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.ArraySet;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.katarzynaiwonakrzemien.domain.contract.user.UserBudgetContract;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.BaseActivity;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.start.StartActivity;

import java.util.Set;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class UserBudgetActivity extends BaseActivity<UserBudgetContract.Presenter, UserBudgetContract.Action, UserBudgetContract.Event>
        implements UserBudgetContract.View, HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> fragmentInjector;

    public static Intent getIntent(Context context) {
        return new Intent(context, UserBudgetActivity.class);
    }

    @Override
    protected void consume(UserBudgetContract.Event event) {
        if (event instanceof UserBudgetContract.Event.RenderSignedInUserName) {
            renderSignedInUserName(((UserBudgetContract.Event.RenderSignedInUserName) event).getName());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_budget);
        setupInfoBar();
        NavController navController = Navigation.findNavController(this, R.id.navigationHost);
        AppBarConfiguration config = new AppBarConfiguration.Builder(getIdNavDestinationSet(navController.getGraph())).build();
        NavigationUI.setupWithNavController((BottomNavigationView) findViewById(R.id.navigationView), navController);
        setSupportActionBar(findViewById(R.id.toolbar));
        NavigationUI.setupActionBarWithNavController(this, navController, config);
    }

    private void setupInfoBar() {
        ((TextView) findViewById(R.id.today)).setText(App.date);
        setupSignedOut();
        send(new UserBudgetContract.Action.FetchSignedInUserName());
    }

    private void setupSignedOut() {
        findViewById(R.id.signOut).setOnClickListener(view -> {
            finishAffinity();
            startActivity(StartActivity.getIntent(UserBudgetActivity.this, true));
        });
    }

    private void renderSignedInUserName(String name) {
        ((TextView) findViewById(R.id.signedInUserName)).setText(name);
    }

    private Set<Integer> getIdNavDestinationSet(NavGraph navGraph) {
        Set<Integer> set = new ArraySet<>();
        for (NavDestination navDestination : navGraph) {
            set.add(navDestination.getId());
        }
        return set;
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return fragmentInjector;
    }
}
