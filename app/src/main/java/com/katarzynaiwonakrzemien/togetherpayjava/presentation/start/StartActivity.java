package com.katarzynaiwonakrzemien.togetherpayjava.presentation.start;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.katarzynaiwonakrzemien.domain.contract.start.StartContract;
import com.katarzynaiwonakrzemien.togetherpayjava.BaseActivity;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.presentation.user.UserBudgetActivity;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class StartActivity extends BaseActivity<StartContract.Presenter, StartContract.Action, StartContract.Event> implements
        StartContract.View, HasAndroidInjector {

    public static final String KEY_SIGN_OUT = "sign_out";
    public static final int RC_SIGN_IN = 1;
    @Inject
    DispatchingAndroidInjector<Object> fragmentInjector;
    private Boolean signOut = false;
    private GoogleSignInClient googleClient;

    public static Intent getIntent(Context context, Boolean signOut) {
        Intent intent = new Intent(context, StartActivity.class);
        intent.putExtra(KEY_SIGN_OUT, signOut);
        return intent;
    }

    @Override
    protected void consume(StartContract.Event event) {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        googleClient = GoogleSignIn.getClient(
                this,
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getResources().getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build()
        );
        setupSignOut(savedInstanceState);
        setContentView(R.layout.acitivity_start);
        findViewById(R.id.googleSignIn).setOnClickListener(view -> signIn());
    }

    private void signIn() {
        startActivityForResult(googleClient.getSignInIntent(), RC_SIGN_IN);
    }

    private void signOut() {
        googleClient.signOut();
        send(new StartContract.Action.SignedOutUser());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RC_SIGN_IN) {
            handleSignInResult(GoogleSignIn.getSignedInAccountFromIntent(data));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loggedInFlow(GoogleSignInAccount account) {
        send(new StartContract.Action.AddSignedInUser(account.getId(), account.getDisplayName()));
        startActivity(UserBudgetActivity.getIntent(this));
        finish();
    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            loggedInFlow(Objects.requireNonNull(task.getResult(ApiException.class)));
        } catch (ApiException e) {
            String message;
            if (e.getMessage() == null) message = getString(R.string.errorLogin, e.getMessage());
            else message = getString(R.string.errorSomething);
            Snackbar.make(findViewById(R.id.container), message, Snackbar.LENGTH_LONG).show();
        }
    }

    private void setupSignOut(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_SIGN_OUT)) {
            signOut = savedInstanceState.getBoolean(KEY_SIGN_OUT, false);
        } else {
            signOut = getIntent().getBooleanExtra(KEY_SIGN_OUT, false);
        }

        if (signOut) signOut();
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return fragmentInjector;
    }
}
