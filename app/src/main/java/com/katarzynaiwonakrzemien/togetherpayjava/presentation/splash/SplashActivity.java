package com.katarzynaiwonakrzemien.togetherpayjava.presentation.splash;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.katarzynaiwonakrzemien.togetherpayjava.presentation.start.StartActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(StartActivity.getIntent(this, false));
        finish();
    }
}
