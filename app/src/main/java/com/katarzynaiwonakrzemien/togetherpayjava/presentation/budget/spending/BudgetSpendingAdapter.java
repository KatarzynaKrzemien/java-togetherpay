package com.katarzynaiwonakrzemien.togetherpayjava.presentation.budget.spending;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.katarzynaiwonakrzemien.domain.model.SpendingUi;
import com.katarzynaiwonakrzemien.togetherpayjava.App;
import com.katarzynaiwonakrzemien.togetherpayjava.R;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericListAdapter;
import com.katarzynaiwonakrzemien.togetherpayjava.adapter.GenericViewHolder;

public class BudgetSpendingAdapter extends GenericListAdapter<SpendingUi> {

    private final BudgetSpendingActionListener budgetSpendingActionListener;

    public BudgetSpendingAdapter(@NonNull DiffUtil.ItemCallback<SpendingUi> itemCallback, BudgetSpendingActionListener budgetSpendingActionListener) {
        super(itemCallback, R.layout.element_spending);
        this.budgetSpendingActionListener = budgetSpendingActionListener;
    }

    @NonNull
    @Override
    public GenericViewHolder<SpendingUi> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BudgetSpendingViewHolder(super.inflate(parent), budgetSpendingActionListener);
    }

    class BudgetSpendingViewHolder extends GenericViewHolder<SpendingUi> {
        private final BudgetSpendingActionListener budgetSpendingActionListener;
        private TextView person = itemView.findViewById(R.id.person);
        private TextView amount = itemView.findViewById(R.id.amount);
        private TextView date = itemView.findViewById(R.id.date);
        private TextView product = itemView.findViewById(R.id.product);
        private TypedArray typedArray = itemView.getContext().getTheme().obtainStyledAttributes(R.styleable.ViewStyle);

        public BudgetSpendingViewHolder(@NonNull View itemView, BudgetSpendingActionListener budgetSpendingActionListener) {
            super(itemView);
            this.budgetSpendingActionListener = budgetSpendingActionListener;
        }

        @Override
        public void bindTo(final SpendingUi item) {
            person.setText(getPerson(item));
            amount.setText(App.doubleToRoundedString(item.getAmount().getAmount()));
            date.setText(item.getDate().getTime());
            product.setText(item.getProduct());
            itemView.setOnClickListener(view -> budgetSpendingActionListener.onSpendingClicked(item, view));
            itemView.setBackgroundColor(typedArray.getColor(getAdjustViewColor(item), Color.TRANSPARENT));
        }

        private String getPerson(SpendingUi item) {
            if (item.getAmount().getBelongToSignInUser()) {
                return itemView.getContext().getString(R.string.you);
            } else {
                return item.getUser().getName();
            }
        }

        private int getAdjustViewColor(SpendingUi item) {
            if (item.getAmount().getBelongToSignInUser()) {
                return R.styleable.ViewStyle_userSpendingBackground;
            } else {
                return R.styleable.ViewStyle_otherUserSpendingBackground;
            }
        }
    }

    interface BudgetSpendingActionListener {
        void onSpendingClicked(SpendingUi spending, View sharedView);
    }
}
