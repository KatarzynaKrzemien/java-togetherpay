package com.katarzynaiwonakrzemien.togetherpayjava.user;

import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;

public class FirebaseSignedInUser implements SignedInUser {


    private Signed state = Signed.OUT;
    private User user = new User();

    @Override
    public void signOut() {
        state = Signed.OUT;
    }

    @Override
    public void signIn(User user) {
        this.user = user;
        state = Signed.IN;
    }

    @Override
    public User getUser() {
        if (state == Signed.IN) {
            return user;
        } else {
            throw new IllegalAccessError();
        }
    }

    enum Signed {
        IN, OUT
    }
}
