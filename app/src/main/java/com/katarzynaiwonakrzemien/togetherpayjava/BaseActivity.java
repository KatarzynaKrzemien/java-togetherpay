package com.katarzynaiwonakrzemien.togetherpayjava;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BaseContract;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public abstract class BaseActivity<P extends BaseContract.Presenter<E, A>, A extends BaseContract.Action, E extends BaseContract.Event>
        extends AppCompatActivity implements BaseContract.View {

    @Inject
    P presenter;

    @Inject
    RxSchedulers schedulers;

    private final CompositeDisposable disposables = new CompositeDisposable();

    protected final Subject<A> actionSubject = PublishSubject.create();

    protected abstract void consume(E event);

    protected void send(A action) {
        actionSubject.onNext(action);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        presenter.attach(actionSubject);
        addDisposable(presenter.subscribe()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(new Consumer<E>() {
                    @Override
                    public void accept(E e) throws Exception {
                        consume(e);
                    }
                }));
        super.onCreate(savedInstanceState);
    }

    boolean addDisposable(Disposable disposable) {
        return disposables.add(disposable);
    }

    @Override
    protected void onDestroy() {
        presenter.dispose();
        disposables.clear();
        super.onDestroy();
    }
}
