package com.katarzynaiwonakrzemien.togetherpayjava;

import android.app.Application;

import com.katarzynaiwonakrzemien.togetherpayjava.di.AppComponent;
import com.katarzynaiwonakrzemien.togetherpayjava.di.DaggerAppComponent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class App extends Application implements HasAndroidInjector {

    public static String date = new SimpleDateFormat("EEE | dd MMM yyyy", Locale.getDefault())
            .format(Calendar.getInstance().getTime());

    public static String doubleToRoundedString(double value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_UP).toString();
    }

    @Inject
    DispatchingAndroidInjector<Object> dispatchingActivityInjector;

    private final AppComponent appComponent = DaggerAppComponent.builder().appContext(this).build();

    @Override
    public void onCreate() {
        appComponent.inject(this);
        super.onCreate();
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return dispatchingActivityInjector;
    }
}
