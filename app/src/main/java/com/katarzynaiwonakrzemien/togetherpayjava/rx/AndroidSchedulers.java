package com.katarzynaiwonakrzemien.togetherpayjava.rx;

import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class AndroidSchedulers implements RxSchedulers {
    @Override
    public Scheduler getBackground() {
        return Schedulers.io();
    }

    @Override
    public Scheduler getUi() {
        return io.reactivex.android.schedulers.AndroidSchedulers.mainThread();
    }
}
