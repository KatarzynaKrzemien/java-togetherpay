package com.katarzynaiwonakrzemien.togetherpayjava.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

public abstract class GenericListAdapter<T> extends ListAdapter<T, GenericViewHolder<T>> {

    @LayoutRes
    private int layoutRes;

    public GenericListAdapter(@NonNull DiffUtil.ItemCallback<T> itemCallback, int layoutRes) {
        super(itemCallback);
        this.layoutRes = layoutRes;
    }

    protected View inflate(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    @Override
    public void onBindViewHolder(@NonNull GenericViewHolder<T> holder, int position) {
        holder.bindTo(getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        return layoutRes;
    }
}
