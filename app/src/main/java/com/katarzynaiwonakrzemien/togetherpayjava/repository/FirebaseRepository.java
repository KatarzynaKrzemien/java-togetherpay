package com.katarzynaiwonakrzemien.togetherpayjava.repository;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.repository.Repository;
import com.katarzynaiwonakrzemien.domain.user.SignedInUser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

public class FirebaseRepository implements Repository {

    private long intervalPeriod = 60L;
    private long intervalStart = 0L;
    private String USERS = "users";
    private String BUDGETS = "budgets";
    private String SPENDING = "spending";
    private String REPAID_DEBTS = "repaidDebts";
    private FirebaseFirestore firebaseFirestore;
    private SignedInUser signedInUser;

    private BehaviorSubject<List<User>> usersSubject = BehaviorSubject.create();
    private Observable<List<User>> users = Observable.interval(intervalStart, intervalPeriod, TimeUnit.SECONDS)
            .flatMap(l -> this.toSingle(firebaseFirestore.collection(USERS).get()).map(query -> {
                List<User> usersApi = new ArrayList<>();
                for (DocumentSnapshot document : query.getDocuments()) {
                    usersApi.add(document.toObject(User.class));
                }
                return usersApi;
            }).toObservable());

    private BehaviorSubject<List<Budget>> budgetsSubject = BehaviorSubject.create();
    private Observable<List<Budget>> budgets = Observable.interval(intervalStart, intervalPeriod, TimeUnit.SECONDS)
            .flatMap(l -> Observable.zip(
                    this.toSingle(firebaseFirestore.collection(BUDGETS).get()).map(query -> {
                        List<Budget> budgetsApi = new ArrayList<>();
                        for (DocumentSnapshot document : query.getDocuments()) {
                            budgetsApi.add(document.toObject(Budget.class));
                        }
                        return budgetsApi;
                    }).toObservable(),
                    this.toSingle(firebaseFirestore.collection(SPENDING).get()).map(query -> {
                        List<Spending> spendingApi = new ArrayList<>();
                        for (DocumentSnapshot document : query.getDocuments()) {
                            spendingApi.add(document.toObject(Spending.class));
                        }
                        return spendingApi;
                    }).toObservable(),
                    this.toSingle(firebaseFirestore.collection(REPAID_DEBTS).get()).map(query -> {
                        List<Debt> debtsApi = new ArrayList<>();
                        for (DocumentSnapshot document : query.getDocuments()) {
                            debtsApi.add(document.toObject(Debt.class));
                        }
                        return debtsApi;
                    }).toObservable(),
                    (budgets, spendingList, debts) -> {
                        List<Budget> budgetsApi = new ArrayList<>();
                        for (Budget budget : budgets) {
                            if (budget.getFirstUserId().equals(signedInUser.getUser().getId()) || budget.getSecondUserId().equals(signedInUser.getUser().getId())) {
                                List<Spending> budgetSpending = new ArrayList<>();
                                for (Spending spending : spendingList) {
                                    if (budget.getId().equals(spending.getIdBudget())) {
                                        budgetSpending.add(spending);
                                    }
                                }
                                List<Debt> budgetRepaidDebts = new ArrayList<>();
                                for (Debt debt : debts) {
                                    if (budget.getId().equals(debt.getIdBudget())) {
                                        budgetRepaidDebts.add(debt);
                                    }
                                }
                                budgetsApi.add(new Budget(budget.getId(), budget.getName(),
                                        budget.getFirstUserId(), budget.getFirstUserName(),
                                        budget.getSecondUserId(), budget.getSecondUserName(),
                                        budgetSpending, budgetRepaidDebts));
                            }
                        }
                        return budgetsApi;
                    }
            ));

    public FirebaseRepository(FirebaseFirestore firebaseFirestore, SignedInUser signedInUser) {
        this.firebaseFirestore = firebaseFirestore;
        this.signedInUser = signedInUser;
    }

    @Override
    public Observable<List<Budget>> budgets() {
        budgets.subscribe(budgets -> budgetsSubject.onNext(budgets));
        return budgetsSubject;
    }

    @Override
    public Observable<List<User>> users() {
        users.subscribe(users -> usersSubject.onNext(users));
        return usersSubject;
    }

    @Override
    public void addUserIfNotExist(User user) {
        firebaseFirestore
                .collection(USERS).document(user.getId())
                .set(user.toHashMap(), SetOptions.merge());
    }

    @Override
    public void addBudget(Budget budget) {
        firebaseFirestore
                .collection(BUDGETS).document(budget.getId())
                .set(budget.toHashMap(), SetOptions.merge());
        refreshBudgets();
    }

    @Override
    public void addSpending(Spending spending) {
        firebaseFirestore
                .collection(SPENDING).document(spending.getId())
                .set(spending.toHashMap(), SetOptions.merge());
        refreshBudgets();
    }

    @Override
    public void addDebt(Debt debt) {
        firebaseFirestore
                .collection(REPAID_DEBTS).document(debt.getId())
                .set(debt.toHashMap(), SetOptions.merge());
        refreshBudgets();
    }

    @Override
    public void deleteBudget(Budget budget) {
        for (Spending spending : budget.getSpending()) {
            firebaseFirestore.collection(SPENDING).document(spending.getId()).delete();
        }
        for (Debt debt : budget.getRepaidDebts()) {
            firebaseFirestore.collection(REPAID_DEBTS).document(debt.getId()).delete();
        }
        firebaseFirestore.collection(BUDGETS).document(budget.getId()).delete();
        refreshBudgets();
    }

    @Override
    public void deleteSpending(String id) {
        firebaseFirestore.collection(SPENDING).document(id).delete();
        refreshBudgets();
    }

    @Override
    public void deleteDebt(String id) {
        firebaseFirestore.collection(REPAID_DEBTS).document(id).delete();
        refreshBudgets();
    }

    @Override
    public void updateDebt(Debt debt) {
        firebaseFirestore
                .collection(REPAID_DEBTS)
                .document(debt.getId())
                .set(debt.toHashMap(), SetOptions.merge());
        refreshBudgets();
    }

    @Override
    public void updateBudget(Budget budget) {
        firebaseFirestore
                .collection(BUDGETS)
                .document(budget.getId())
                .set(budget.toHashMap(), SetOptions.merge());
        refreshBudgets();
    }

    @Override
    public void updateSpending(Spending spending) {
        firebaseFirestore
                .collection(SPENDING)
                .document(spending.getId())
                .set(spending.toHashMap(), SetOptions.merge());
        refreshBudgets();
    }

    @Override
    public void refreshBudgets() {
        budgets.subscribe(budgets -> budgetsSubject.onNext(budgets));
    }

    @Override
    public void refreshUsers() {
        users.subscribe(users -> usersSubject.onNext(users));
    }

    private <T> Single<T> toSingle(Task<T> task) {
        return Single.create(emitter -> {
            task.addOnSuccessListener(emitter::onSuccess);
            task.addOnFailureListener(emitter::onError);
        });
    }
}
