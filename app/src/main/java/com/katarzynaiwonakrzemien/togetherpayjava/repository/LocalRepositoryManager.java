package com.katarzynaiwonakrzemien.togetherpayjava.repository;

import com.katarzynaiwonakrzemien.core.model.Budget;
import com.katarzynaiwonakrzemien.core.model.Debt;
import com.katarzynaiwonakrzemien.core.model.Spending;
import com.katarzynaiwonakrzemien.core.model.User;
import com.katarzynaiwonakrzemien.core.repository.Repository;
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager;

import java.util.List;

import io.reactivex.Observable;

public class LocalRepositoryManager implements RepositoryManager {

    private Repository repository;
    private Observable<List<User>> users = null;
    private Observable<List<Budget>> budgets = null;

    public LocalRepositoryManager(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void cleanData() {
        users = null;
        budgets = null;
    }

    @Override
    public Observable<List<Budget>> budgets() {
        if (budgets == null) budgets = repository.budgets();
        return budgets;
    }

    @Override
    public Observable<List<User>> users() {
        if (users == null) users = repository.users();
        return users;
    }

    @Override
    public void addUserIfNotExist(User user) {
        repository.addUserIfNotExist(user);
    }

    @Override
    public void addBudget(Budget budget) {
        repository.addBudget(budget);
    }

    @Override
    public void addSpending(Spending spending) {
        repository.addSpending(spending);
    }

    @Override
    public void addDebt(Debt debt) {
        repository.addDebt(debt);
    }

    @Override
    public void deleteBudget(Budget budget) {
        repository.deleteBudget(budget);
    }

    @Override
    public void deleteSpending(String id) {
        repository.deleteSpending(id);
    }

    @Override
    public void deleteDebt(String id) {
        repository.deleteDebt(id);
    }

    @Override
    public void updateDebt(Debt debt) {
        repository.updateDebt(debt);
    }

    @Override
    public void updateBudget(Budget budget) {
        repository.updateBudget(budget);
    }

    @Override
    public void updateSpending(Spending spending) {
        repository.updateSpending(spending);
    }

    @Override
    public void refreshBudgets() {
        repository.refreshBudgets();
    }

    @Override
    public void refreshUsers() {
        repository.refreshUsers();
    }
}
