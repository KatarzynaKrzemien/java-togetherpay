package com.katarzynaiwonakrzemien.togetherpayjava;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.katarzynaiwonakrzemien.core.rx.RxSchedulers;
import com.katarzynaiwonakrzemien.domain.BaseContract;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public abstract class BaseFragment<P extends BaseContract.Presenter<E, A>, A extends BaseContract.Action, E extends BaseContract.Event>
        extends Fragment implements BaseContract.View {

    @Inject
    P presenter;

    @Inject
    RxSchedulers schedulers;

    private final CompositeDisposable disposables = new CompositeDisposable();
    private final Subject<A> actionSubject = PublishSubject.create();

    protected abstract void consume(E event);

    protected void send(A action) {
        actionSubject.onNext(action);
    }

    protected abstract int getLayoutRes();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
        presenter.attach(actionSubject);
        addDisposable(presenter.subscribe()
                .subscribeOn(schedulers.getBackground())
                .observeOn(schedulers.getUi())
                .subscribe(e -> {
                    if (isVisible()) {
                        consume(e);
                    }
                }));
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutRes(), container, false);
    }

    boolean addDisposable(Disposable disposable) {
        return disposables.add(disposable);
    }

    @Override
    public void onDestroy() {
        presenter.dispose();
        disposables.clear();
        super.onDestroy();
    }
}
