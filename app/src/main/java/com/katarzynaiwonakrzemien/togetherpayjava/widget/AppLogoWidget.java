package com.katarzynaiwonakrzemien.togetherpayjava.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.katarzynaiwonakrzemien.togetherpayjava.R;

public class AppLogoWidget extends ConstraintLayout {

    public AppLogoWidget(Context context) {
        super(context, null, 0);
        init(context);
    }

    public AppLogoWidget(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        init(context);
    }

    public AppLogoWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.widget_app_logo, this, true);
    }
}
